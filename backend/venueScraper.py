import  mysql.connector
import requests
from mysql.connector import Error

try:
    connection = mysql.connector.connect(
        host="aar9pdseju4br9.cgodtpceng2k.us-west-2.rds.amazonaws.com",
        user="admin",
        password="resonance123",
        database="res_db"
    )

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        
        sqlFormula = "INSERT INTO venue (name, address, number, rating, hours, image, type, website) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        venueids = []
        f=open("venueids.txt", "r")
        if f.mode == 'r':
            contents = f.read()
            content = contents.split('\n')
            for c in content:
                venueids.append(c)
        count = 0
        for id in venueids:
            print(id)
            response = requests.get("https://maps.googleapis.com/maps/api/place/details/json?place_id=" + id + "&key=AIzaSyCAfuFCaBanXgdDiwtj7uJAztnWoqU0Qbk")
            json = response.json()
            
            name = "None"
            if "name" in json["result"]:
                name = json["result"]["name"]

            address = "None"
            if "formatted_address" in json["result"]:
                address = json["result"]["formatted_address"]
            
            number = None
            if "formatted_phone_number" in json["result"]:
                number = json["result"]["formatted_phone_number"]

            rating = None
            if "rating" in json["result"]:
                rating = json["result"]["rating"]

            hours = None
            if "opening_hours" in json["result"]:
                hours = json["result"]["opening_hours"]["weekday_text"]

            image = None
            if "photos" in json["result"]:
                s = json["result"]["photos"][3]["html_attributions"][0]
                start = s.index( "\"" ) + 1
                end = s.index( "\"", start )
                image =  s[start:end]
                

            type = None
            if "types" in json["result"]:
                type = json["result"]["types"]                

            website = None        
            if "website" in json["result"]:
                website = json["result"]["website"]    

            # location = None
            # if "location" in json["result"]:
            #     location = json["result"]["location"]
            # print(location)                

            venue = (name, address, number, rating, str(hours), image, type[0], website)
            # cursor.execute(sqlFormula, venue)
            count += 1
        print("Looked at all")



    
        

        
        # artist2 = (1, "test", "d", None, None, None, None, None, None)
        # cursor.execute(sqlFormula, artist2)

        connection.commit()

except Error as e:
    print("Error while connecting to MySQL", e)
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")
