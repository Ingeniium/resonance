import os
import tempfile
import pytest
from flask import Flask
import unittest
from application import application, engine

# Used the tutorial at https://www.patricksoftwareblog.com/unit-testing-a-flask-application/

TEST_DB = "test.db"


class BasicTests(unittest.TestCase):

    def setUp(self):
        application.config['TESTING'] = True
        application.config['WTF_CSRF_ENABLED'] = False
        application.config['DEBUG'] = False
        #application.config['SQLALCHEMY_DATABASE_URI'] = engine
        self.app = application.test_client()

    # executed after each test
    def tearDown(self):
        pass

    def testArtistRange(self):
        response = self.app.get('/api/artists/range/1/43', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        return response.json

    def testEventRange(self):
        response = self.app.get('/api/events/range/1/50', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        return response.json

    def testVenueRange(self):
        response = self.app.get('/api/venues/range/1/27', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        return response.json

    def testOneArtist(self):
        response = self.app.get('/api/artists/name/Eagles', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        return response.json

    def testOneVenue(self):
        response = self.app.get('/api/venues/name/American Airlines Center', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        return response.json

    def testOneEvent(self):
        response = self.app.get('/api/events/name/Eagles', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        return response.json

    def testShortInputE(self):
        response = self.app.get('/api/events/name/Ea', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, self.testOneEvent())

    def testShortInputV(self):
        response = self.app.get('/api/venues/name/American', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, self.testOneVenue())

    def testShortInputA(self):
        response = self.app.get('/api/artists/name/Eag', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, self.testOneArtist())

    def testGuide(self):
        response = self.app.get('/api', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def testArtistList(self):
        response = self.app.get('/api/artists', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, self.testArtistRange())

    def testVenueList(self):
        response = self.app.get('/api/venues', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, self.testVenueRange())

    def testEventList(self):
        response = self.app.get('/api/events', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, self.testEventRange())

    def testAll(self):
        response = self.app.get('/api/all', follow_redirects=True)
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
