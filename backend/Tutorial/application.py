from flask import Flask, jsonify
import sqlalchemy as db
from flask_cors import CORS, cross_origin


engine = db.create_engine('mysql://admin:resonance123@aar9pdseju4br9.cgodtpceng2k.us-west-2.rds.amazonaws.com:3306/res_db')
connection = engine.connect()
metadata = db.MetaData()
application = Flask(__name__)
artist = db.Table('artist', metadata, autoload=True, autoload_with=engine)
venue = db.Table('venue', metadata, autoload=True, autoload_with=engine)
event = db.Table('event', metadata, autoload=True, autoload_with=engine)
CORS(application)


@application.route('/api', methods=['GET'])
@cross_origin()
def guide():
    return ("Possible routes:  1. /category returns all instances in a category as a list of dictionaries\n "
            "2. /category/field/keyword' returns a single instance whose value in the field matches the "
            "keyword\n 3. /category/'range'/start/end returns all instances whose IDs are contained within "
            "the range [start, finish]. Indexing begins at 1, and it is inclusive. \n Note that 'range' is the"
            "word 'range' rather than a variable like the other words")


@application.route('/api/<name>', methods=['GET'])
@cross_origin()
def api(name):
    if name == "artists":
        result = list(artistgenerator())
    elif name == "events":
        result = list(eventgenerator())
    elif name == "venues":
        result = list(venuegenerator())
    elif name == "all":
        result = list(artistgenerator()) + list(venuegenerator()) + list(eventgenerator())
    else:
        return jsonify("error, check input value")
    return jsonify(result)


@application.route('/api/artists/<field>/<keyword>', methods=['GET'])
@cross_origin()
def oneartist(field, keyword):
    field = artist.columns[field]
    keyword = "%" + keyword + "%"
    artistid = connection.execute(db.select([artist.columns.idartist]).where(field.like(keyword))).scalar()
    result = list(artistgenerator(artistid, artistid))
    return jsonify(result)


@application.route('/api/events/<field>/<keyword>', methods=['GET'])
@cross_origin()
def oneevent(field, keyword):
    field = event.columns[field]
    keyword = "%" + keyword + "%"
    eventid = connection.execute(db.select([event.columns.idevent]).where(field.like(keyword))).scalar()
    result = list(eventgenerator(eventid, eventid))
    return jsonify(result)


@application.route('/api/venues/<field>/<keyword>', methods=['GET'])
@cross_origin()
def onevenue(field, keyword):
    field = venue.columns[field]
    keyword = "%" + keyword + "%"
    venueid = connection.execute(db.select([venue.columns.idvenue]).where(field.like(keyword))).scalar()
    result = list(venuegenerator(venueid, venueid))
    return jsonify(result)


@application.route('/api/artists/range/<a>/<b>', methods=['GET'])
@cross_origin()
def artistrange(a, b):
    a = int(a)
    b = int(b)
    result = list(artistgenerator(a, b))
    return jsonify(result)


@application.route('/api/venues/range/<a>/<b>', methods=['GET'])
@cross_origin()
def venuerange(a, b):
    a = int(a)
    b = int(b)
    result = list(venuegenerator(a, b))
    return jsonify(result)


@application.route('/api/events/range/<a>/<b>', methods=['GET'])
@cross_origin()
def eventrange(a, b):
    a = int(a)
    b = int(b)
    result = list(eventgenerator(a, b))
    return jsonify(result)


def artistgenerator(a=1, b=43):
    for x in range(a, b+1):
        result = {"name": "", "genre": "", "idartist": "", "top_songs": "", "image": "", "popularity": "", "albums": "",
                  "spotifyurl": "", "event": "", "venue": "", "youtube": ""}
        nam = db.select([artist.columns.name]).where(artist.columns.idartist >= x)
        result["name"] = connection.execute(nam).scalar()
        if a == b:
            events = db.select([event.columns.name]).where(event.columns.artist.like("%" + result["name"] + "%"))
            venues = db.select([event.columns.venue]).where(event.columns.artist.like("%" + result["name"] + "%"))
            result["event"] = connection.execute(events).scalar()
            result["venue"] = connection.execute(venues).scalar()
        gen = db.select([artist.columns.genre]).where(artist.columns.idartist >= x)
        result["genre"] = connection.execute(gen).scalar()
        id = db.select([artist.columns.idartist]).where(artist.columns.idartist >= x)
        result["idartist"] = connection.execute(id).scalar()
        ts = db.select([artist.columns.top_songs]).where(artist.columns.idartist >= x)
        result["top_songs"] = connection.execute(ts).scalar()
        img = db.select([artist.columns.image]).where(artist.columns.idartist >= x)
        result["image"] = connection.execute(img).scalar()
        pop = db.select([artist.columns.popularity]).where(artist.columns.idartist >= x)
        result["popularity"] = connection.execute(pop).scalar()
        alb = db.select([artist.columns.albums]).where(artist.columns.idartist >= x)
        result["albums"] = connection.execute(alb).scalar()
        surl = db.select([artist.columns.spotifyurl]).where(artist.columns.idartist >= x)
        result["spotifyurl"] = connection.execute(surl).scalar()
        result["youtube"] = youtube(result["idartist"])
        x += 1
        yield result


def eventgenerator(a=1, b=50):
    for x in range(a, b+1):
        result = {"name": "", "date": "", "idevent": "", "time": "", "images": "", "price": "", "genre": "",
                  "subGenre": "", "url": "", "venue": "", "artist": "", "latitude": "", "longitude": ""}
        nam = db.select([event.columns.name]).where(event.columns.idevent == x)
        result["name"] = connection.execute(nam).scalar()
        gen = db.select([event.columns.genre]).where(event.columns.idevent == x)
        result["genre"] = connection.execute(gen).scalar()
        id = db.select([event.columns.idevent]).where(event.columns.idevent == x)
        result["idevent"] = connection.execute(id).scalar()
        ts = db.select([event.columns.time]).where(event.columns.idevent == x)
        result["time"] = connection.execute(ts).scalar()
        img = db.select([event.columns.images]).where(event.columns.idevent == x)
        result["images"] = connection.execute(img).scalar()
        pop = db.select([event.columns.price]).where(event.columns.idevent == x)
        result["price"] = connection.execute(pop).scalar()
        alb = db.select([event.columns.date]).where(event.columns.idevent == x)
        result["date"] = connection.execute(alb).scalar()
        surl = db.select([event.columns.subGenre]).where(event.columns.idevent == x)
        result["subGenre"] = connection.execute(surl).scalar()
        url = db.select([event.columns.url]).where(event.columns.idevent == x)
        result["url"] = connection.execute(url).scalar()
        ven = db.select([event.columns.venue]).where(event.columns.idevent == x)
        result["venue"] = connection.execute(ven).scalar()
        art = db.select([event.columns.artist]).where(event.columns.idevent == x)
        result["artist"] = connection.execute(art).scalar()
        lat = db.select([event.columns.latitude]).where(event.columns.idevent == x)
        result["latitude"] = connection.execute(lat).scalar()
        long = db.select([event.columns.longitude]).where(event.columns.idevent == x)
        result["longitude"] = connection.execute(long).scalar()
        x += 1
        yield result


def venuegenerator(a=1, b=27):
    for x in range(a, b+1):
        result = {"name": "", "address": "", "number": "", "rating": "", "image": "", "hours": "", "type": "",
                  "website": "", "idvenue": "", "event": "", "artist": "", "latitude": "", "longitude": ""}
        nam = db.select([venue.columns.name]).where(venue.columns.idvenue == x)
        result["name"] = connection.execute(nam).scalar()
        if a == b:
            events = db.select([event.columns.name]).where(event.columns.venue.like("%" + result["name"]+"%"))
            artists = db.select([event.columns.artist]).where(event.columns.venue.like("%" + result["name"]+"%"))
            lat = db.select([event.columns.latitude]).where(event.columns.venue.like("%" + result["name"] + "%"))
            long = db.select([event.columns.longitude]).where(event.columns.venue.like("%" + result["name"] + "%"))
            result["event"] = connection.execute(events).scalar()
            result["artist"] = connection.execute(artists).scalar()
            result["latitude"] = connection.execute(lat).scalar()
            result["longitude"] = connection.execute(long).scalar()
        gen = db.select([venue.columns.address]).where(venue.columns.idvenue == x)
        result["address"] = connection.execute(gen).scalar()
        num = db.select([venue.columns.number]).where(venue.columns.idvenue == x)
        result["number"] = connection.execute(num).scalar()
        ts = db.select([venue.columns.rating]).where(venue.columns.idvenue == x)
        result["rating"] = str(connection.execute(ts).scalar())
        img = db.select([venue.columns.image]).where(venue.columns.idvenue == x)
        result["image"] = connection.execute(img).scalar()
        pop = db.select([venue.columns.hours]).where(venue.columns.idvenue == x)
        result["hours"] = connection.execute(pop).scalar()
        alb = db.select([venue.columns.type]).where(venue.columns.idvenue == x)
        result["type"] = connection.execute(alb).scalar()
        surl = db.select([venue.columns.website]).where(venue.columns.idvenue == x)
        result["website"] = connection.execute(surl).scalar()
        result["idvenue"] = x
        x += 1
        yield result


def youtube(idartist):
    youtubelist = ['https://www.youtube.com/embed/nXJIVAy7kCM', 'https://www.youtube.com/embed/WNIPqafd4As', 'https://www.youtube.com/embed/nqK2cD1SbOg', 'https://www.youtube.com/embed/30zWND-MWkM', 'https://www.youtube.com/embed/ap14O5-G7UA', 'https://www.youtube.com/embed/Soa3gO7tL-c', 'https://www.youtube.com/embed/CnAmeh0-E-U', 'https://www.youtube.com/embed/Ypkv0HeUvTc', 'https://www.youtube.com/embed/PT2_F-1esPk', 'https://www.youtube.com/embed/V-plAPYbQvg', 'https://www.youtube.com/embed/eJnQBXmZ7Ek', 'https://www.youtube.com/embed/CN4IIgFz93k', 'https://www.youtube.com/embed/gl1aHhXnN1k', 'https://www.youtube.com/embed/aa3nij927Ig',  'https://www.youtube.com/embed/BsKbwR7WXN4', 'https://www.youtube.com/embed/e4ujS1er1r0', 'https://www.youtube.com/embed/4cP26ndrmtg', 'https://www.youtube.com/embed/SDSXLkD74KE', 'https://www.youtube.com/embed/ng7QrYgdItQ', 'https://www.youtube.com/embed/PF06CnRSr1s', 'https://www.youtube.com/embed/XGGWhOUYObc', 'https://www.youtube.com/embed/Pi9J4epTWGM', 'https://www.youtube.com/embed/Y2E71oe0aSM', 'https://www.youtube.com/embed/mpaPBCBjSVc', 'https://www.youtube.com/embed/FGIfE1SjLC0', 'https://www.youtube.com/embed/jHnSj9Ls6pU', 'https://www.youtube.com/embed/rPFoMvooWs0', 'https://www.youtube.com/embed/VMldAVhCBm8', 'https://www.youtube.com/embed/rkRdgFvuiYk', 'https://www.youtube.com/embed/cT1Kzk7akjQ', 'https://www.youtube.com/embed/2c4dB52ehAE', 'https://www.youtube.com/embed/fKopy74weus', 'https://www.youtube.com/embed/J1OsKJW51HY', 'https://www.youtube.com/embed/OIKC2ggCed4', 'https://www.youtube.com/embed/q7DfQMPmJRI', 'https://www.youtube.com/embed/9jJf-p6RYvo', 'https://www.youtube.com/embed/OSUxrSe5GbI', 'https://www.youtube.com/embed/wCWYZHYIPyE', 'https://www.youtube.com/embed/p8NQUbLQGio', 'https://www.youtube.com/embed/17fnUqLdm7o', 'https://www.youtube.com/embed/pAnK1y7qjuE', 'https://www.youtube.com/embed/DcnNeGQHwDc', 'https://www.youtube.com/embed/1YT5Sl7D9SA']
    return youtubelist[int(idartist) - 1]


if __name__ == '__main__':
    application.run()
