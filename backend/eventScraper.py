import  mysql.connector
import  mysql.connector
import requests
from mysql.connector import Error

try:
    connection = mysql.connector.connect(
        host="aar9pdseju4br9.cgodtpceng2k.us-west-2.rds.amazonaws.com",
        user="admin",
        password="resonance123",
        database="res_db"
    )

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        
        sqlFormula = "INSERT INT Oevent (name, time, date, price, genre, subGenre, images, url, venue, artist, latitude, longitude) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # sqlFormula1 = "UPDATE event  SET latitude = %s WHERE name = %s"
        eventids = []
        f=open("eventids.txt", "r")
        if f.mode == 'r':
            contents = f.read()
            content = contents.split('\n')
            for c in content:
                eventids.append(c.strip())
        count = 0
        for id in eventids:
            count += 1
            print(count)
            response = requests.get("https://app.ticketmaster.com/discovery/v2/events/" + id + ".json?apikey=OF8V7fQ04Z5gI16j1oWR4hIBNrVda8Vt")
            json = response.json()
            # print(json)
            name = "None"
            if "name" in json:
                name = json["name"]

            startTime = None
            if "dates" in json and "start" in json["dates"] and "localTime" in json["dates"]["start"]:
                startTime = json["dates"]["start"]["localTime"]
            
            startDate = None
            if "dates" in json and "start" in json["dates"] and "localDate" in json["dates"]["start"]:
                startDate = json["dates"]["start"]["localDate"]
        
            min = None
            if "priceRanges" in json and "min" in json["priceRanges"][0]:
               min = json["priceRanges"][0]["min"]

            max = None
            if "priceRanges" in json and "max" in json["priceRanges"][0]:
               max = json["priceRanges"][0]["max"]
               
            genre = None
            if "classifications" in json and "genre" in json["classifications"][0] and "name" in json["classifications"][0]["genre"]:
                genre = json["classifications"][0]["genre"]["name"]
            
            subGenre = None
            if  "classifications" in json and "subGenre" in json["classifications"][0] and "name" in json["classifications"][0]["subGenre"]:
                subGenre = json["classifications"][0]["subGenre"]["name"]
                
            images = None
            if "images" in json:
                images = json["images"][0]["url"]  

            # print(images)        

            url = None     
            if "url" in json:
                url = json["url"]    

            price = None
            if min and max:
                price = str(min) + " - "  + str(max)
            
            venue = None
            if "_embedded" in json and "venues" in json["_embedded"]:
                venue = json["_embedded"]["venues"][0]["name"]

            artist = None
            if "_embedded" in json and "attractions" in json["_embedded"]:
                artist = json["_embedded"]["attractions"][0]["name"]    

            location = None
            latitude = None
            longitude = None
            if "location" in json["_embedded"]["venues"][0]:
                location = json["_embedded"]["venues"][0]["location"] 
                latitude = location["latitude"]
                longitude = location["longitude"]
            # print(name)
            # print(startTime)
            # print(startDate)
            # print(price)
            # print(genre)
            # print(subGenre)
            # print(images)
            # print(url)
            # print(venue)
            # print(artist)
            # print(latitude)
            # print(longitude)
                
            event = (name, startTime, startDate, price, genre, subGenre, images, url, venue, artist, latitude, longitude)
            # event = (latitude, name)
            # print(event)
            cursor.execute(sqlFormula, event)
            # count += 1
        print("Looked at all")



    
        

        
        # artist2 = (1, "test", "d", None, None, None, None, None, None)
        # cursor.execute(sqlFormula, artist2)

        connection.commit()

except Error as e:
    print("Error while connecting to MySQL", e)
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")
