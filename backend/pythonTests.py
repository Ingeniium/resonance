#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from venueScraper import connect_to_database1, read_ids1, get_data_to_insert1
from eventScraper import connect_to_database2, read_ids2, get_data_to_insert2
from spotifyScraper import connect_to_database3, read_ids3, get_data_to_insert3

# -----------
# TestCollatz
# -----------


class Test1(TestCase):
    # ----
    # read
    # ----

    def test_connect1(self):
        self.assertEqual(True, connect_to_database1()[1])

    def test_connect2(self):
        self.assertEqual(True, connect_to_database2()[1])

    def test_connect3(self):
        self.assertEqual(True, connect_to_database3()[1])    

    def test_read1(self):
        ids = read_ids1("venueids.txt")
        self.assertEqual(27, len(ids))    
        self.assertEqual(True, len(ids[0])>0)    

    def test_read2(self):
        ids = read_ids2("eventids.txt")
        self.assertEqual(54, len(ids))    
        self.assertEqual(True, len(ids[0])>0)    

    def test_read3(self):
        ids = read_ids3("venueids.txt")
        self.assertEqual(27, len(ids))    
        self.assertEqual(True, len(ids[0])>0)  

    def test_getdata1_1(self):
        id = read_ids1("venueids.txt")[1]
        data = get_data_to_insert1(id)
        self.assertEqual(type("name"), type(data[0]))
        self.assertEqual(type("name"), type(data[1]))
        self.assertEqual(type("name"), type(data[2]))
        self.assertEqual(type(5.0), type(data[3]))
    
    
    def test_getdata1_2(self):
        id = read_ids1("venueids.txt")[1]
        data = get_data_to_insert1(id)    
        self.assertEqual(type("name"), type(data[4]))
        self.assertEqual(type("name"), type(data[5]))  
        self.assertEqual(type("name"), type(data[6]))  
        self.assertEqual(type("name"), type(data[7]))

    def test_getdata2_1(self):
        id = read_ids2("eventids.txt")[0]
        data = get_data_to_insert2(id)
        self.assertEqual(type("name"), type(data[0]))
        self.assertEqual(type("name"), type(data[1]))
        self.assertEqual(type("name"), type(data[2]))
        self.assertEqual(type("name"), type(data[2]))
        self.assertEqual(type("name"), type(data[4]))

    def test_getdata2_2(self):
        id = read_ids2("eventids.txt")[0]
        data = get_data_to_insert2(id)    
        self.assertEqual(type("name"), type(data[5]))  
        self.assertEqual(type("name"), type(data[6]))  
        self.assertEqual(type("name"), type(data[7]))
        self.assertEqual(type("name"), type(data[8]))
        self.assertEqual(type("name"), type(data[9]))

    
        
 
if __name__ == "__main__":  # pragma: no cover
    main()
