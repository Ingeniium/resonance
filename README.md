# [Resonance](http://resonance-music.me)

**A website for connecting users to concerts in their area and other users of similar music tastes.**

**For the most up-to-date changes to our website, please use our AWS Elastic Beanstalk [endpoint](http://resonance-env.2mer2gbpmm.us-west-2.elasticbeanstalk.com/)**

## Members

* **Anwesh Basu**
  * EID: ab63383
  * GitLab ID: @AnweshBasu
* **Mahir Karim**
  * EID: mak4222
  * GitLab ID: @mahirkarim
* **Kai Lee**
  * EID: hl24932
  * GitLab ID: @kai_lee
* **Andrew Olumbe**
  * EID: abo396
  * GitLab ID: @Ingeniium
* **William Wang**
  * EID: wtw469
  * GitLab ID: @williamtzw

## Git
### [SHA](298f9e3d079f653e8294b91782d6a63e48b8d6e4)
### [Pipeline](https://gitlab.com/Ingeniium/resonance/pipelines)
## Script for Selenium Tests
#### npm run s-test

## Estimated and actual completion time in hours

### Phase I
* **Anwesh Basu**
  * Estimated time to completion: 10
  * Actual time to completion: 20 
* **Mahir Karim**
  * Estimated time to completion: 10
  * Actual time to completion: 10
* **Kai Lee**
  * Estimated time to completion: 10 
  * Actual time to completion: 10 
* **Andrew Olumbe**
  * Estimated time to completion: 20
  * Actual time to completion: 40
* **William Wang (project lead)**
  * Estimated time to completion: 12 
  * Actual time to completion: 20 

### Phase II
* **Andrew Olumbe (project lead)**
  * Estimated time to completion: 20
  * Actual time to completion: 40
* **Anwesh Basu**
  * Estimated time to completion: 20
  * Actual time to completion: 30
* **Kai Lee**
  * Estimated time to completion: 20
  * Actual time to completion: 20
* **Mahir Karim**
  * Estimated time to completion: 15
  * Actual time to completion: 25
* **William Wang**
  * Estimated time to completion: 20
  * Actual time to completion: 40

### Phase III
* **Andrew Olumbe**
  * Estimated time to completion: 15
  * Actual time to completion: 35
* **Anwesh Basu (project lead)**
  * Estimated time to completion: 20
  * Actual time to completion: 25
* **Kai Lee**
  * Estimated time to completion: 15
  * Actual time to completion: 20
* **Mahir Karim**
  * Estimated time to completion: 10
  * Actual time to completion: 25
* **William Wang**
  * Estimated time to completion: 20
  * Actual time to completion: 30

### Phase IV
* **Andrew Olumbe**
  * Estimated time to completion: 10
  * Actual time to completion: 10
* **Anwesh Basu**
  * Estimated time to completion: 10
  * Actual time to completion: 10
* **Kai Lee**
  * Estimated time to completion: 15
  * Actual time to completion: 20
* **Mahir Karim (project lead)**
  * Estimated time to completion: 15
  * Actual time to completion: 15
* **William Wang**
  * Estimated time to completion: 10
  * Actual time to completion: 10