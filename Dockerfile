FROM node:latest

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY frontend/package.json /usr/src/app

RUN npm install

ADD frontend/src /usr/src/app/src
ADD frontend/public /usr/src/app/public

RUN npm run build

EXPOSE 3000

CMD ["npm", "start"]
