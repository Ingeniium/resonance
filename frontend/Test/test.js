/**
 * Dependency Modules
 */
var assert = require("assert").strict;
var webdriver = require("selenium-webdriver");
//require("geckodriver");// Application Server
const serverUri = "http://resonance-music.me/";
const serverTestUri = "http://resonance-env.2mer2gbpmm.us-west-2.elasticbeanstalk.com/";
const appTitle = "Resonance";/**
 * Config for Chrome browser
 * @type {webdriver}
 */
var browser = new webdriver.Builder()
    .usingServer()
    .withCapabilities({ browserName: "chrome" })
    .build();/**
 * Config for Firefox browser (Comment Chrome config when you intent to test in Firefox)
 * @type {webdriver}
 */
/*
var browser = new webdriver.Builder()
 .usingServer()
 .withCapabilities({ browserName: "firefox" })
 .build();
 *//**
* Function to get the title and resolve it it promise.
* @return {[type]} [description]
*/
async function logTitle() {
    return new Promise((resolve, reject) => {
        browser.getTitle().then(function (title) {
            resolve(title);
        });
    });
}



/**
 * Sample test case
 * To check whether the given value is present in array.
 */

describe("Home Page", async function () {
    /**
     * Test case to load our application and check the title.
     */
    it("Should load the home page and get title", function () {
        return new Promise((resolve, reject) => {
            this.timeout(5000);
            browser.wait
            browser
                .get(serverTestUri)
                .then(logTitle)
                .then(title => {
                    assert.strictEqual(title, appTitle);
                    resolve();
                })
                .catch(err => reject(err));
        });
    });
    /**
  * Test case to check whether the given element is loaded.
  */
    it("Go to About Page", async function () {
        return new Promise((resolve, reject) => {
            this.timeout(5000);
            browser.get(serverTestUri + "about");
            browser
                .wait(webdriver.until.elementLocated({ className: "About" }))
                .then(elem => resolve())
                .catch(err => reject(err));
        });
    });

    it("Go to Artist Page", function () {
        return new Promise((resolve, reject) => {
            this.timeout(5000);
            browser.get(serverTestUri + "artists")
                .then(resolve())
                .catch(err => reject(err));
        });
    });
    it("Go to Events Page", function () {
        return new Promise((resolve, reject) => {
            this.timeout(5000);
            browser.get(serverTestUri + "events")
                .then(resolve())
                .catch(err => reject(err));
        });
    });
    it("Go to Venues Page", function () {
        return new Promise((resolve, reject) => {
            this.timeout(5000);
            browser.get(serverTestUri + "venues")
            .then(resolve())
                .catch(err => reject(err));
        });
    });
    it("Go to Eagles Page", function () {
        return new Promise((resolve, reject) => {
            this.timeout(5000);
            browser.get(serverTestUri + "artists/Eagles")
                .then(resolve())
                .catch(err => reject(err));
        });
    });
    it("Go to Eagles Page", function () {
        return new Promise((resolve, reject) => {
            this.timeout(5000);
            browser.get(serverTestUri + "artists/SEVENTEEN")
                .then(resolve())
                .catch(err => reject(err));
        });
    }); 
    it("Go to Tool Page", function () {
        return new Promise((resolve, reject) => {
            this.timeout(5000);
            browser.get(serverTestUri + "events/tool")
                .then(resolve())
                .catch(err => reject(err));
        });
    });
    it("Go to Tool Page", function () {
        return new Promise((resolve, reject) => {
            this.timeout(5000);
            browser.get(serverTestUri + "events/How % 20The % 20Southwest % 20Was % 20Won")
                .then(resolve())
                .catch(err => reject(err));
        });
    });
    it("Go to Alamodome Page", function () {
        return new Promise((resolve, reject) => {
            this.timeout(5000);
            browser.get(serverTestUri + "events/Alamodome")
                .then(resolve())
                .catch(err => reject(err));
        });
    });
    it("Go to Alamodome Page", function () {
        return new Promise((resolve, reject) => {
            this.timeout(5000);
            browser.get(serverTestUri + "events/House%20of%20Blues%20Dallas")
                .then(resolve())
                .catch(err => reject(err));
        });
    });
    

    /**
  * End of test cases use.
  * Closing the browser and exit.
  */
    after(function () {
        // End of test use this.
        browser.quit();
    });
});