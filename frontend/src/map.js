import React, { Component } from 'react'
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';



const mapStyles = {
  width: '95%',
  height: '30%'
};

export class MapContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lat: this.props.lat,
      lng: this.props.lng
    }
  }


  render() {
    return (
        <Map
          google={this.props.google}
          zoom={14}
          style={mapStyles}
          initialCenter={{ lat: this.state.lat, lng: this.state.lng}}
        >
          <Marker position={{ lat: this.state.lat, lng: this.state.lng}} />
        </Map>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyDhhUQBGWZX8rV3PCfmPhS9e5GELvtYymU'
})(MapContainer);