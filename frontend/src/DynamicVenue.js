import React, { Component } from 'react'

import {
    Card, CardImg, CardText, CardBody, CardHeader,
        CardTitle, CardSubtitle, CardLink, Button, Row, Col, ButtonToolbar, ListGroup, ListGroupItem
} from 'reactstrap';
import MapContainer from './map'

export default class DynamicVenue extends React.Component {

	constructor(props) {
		super(props)
	}


	async componentDidMount() {
		var url = this.props.domain + "/events/venue/" + this.props.params
		await this.props.fetchData(url)
	}

	/* Using an array of size 2 gives us a nice way to
	simulate conditional rendering involving links to
	other pages;If there is no valid venue object associated
	with an artist, for example, no words are displayed,
	nor will a link to anything be present. If there is, both
	a sentence and a link to the appropriate page appear in tandem. */
	venueLinks(state) {
		let venue = state.venues
		var links = []
		links.concat(null)
		links.concat(null)
		if (venue.website && venue.website != "") {
			links[0] = venue.website
			links[1] = venue.name
		}
		return links
	}

	artistLinks(state) {
		let artist = state.artists
		var links = []
		links.concat(null)
		links.concat(null)
		if (artist && artist.name && artist.name != "") {
			links[0] = "/artists/" + artist.name
			links[1] = artist.name
		}
		return links
	}

	eventLinks(state) {
		let event = state.events
		var links = []
		links.concat(null)
		links.concat(null)
		if (event && event.name && event.name != "") {
			links[0] = "/events/" + event.name
			links[1] = event.name
		}
		return links
	}

	artistPic(state) {
		if (state.artists) {
			return state.artists.image
		}
    }

	render() {
		let state = this.props.getState();
		if (!state.loaded) {
			return (<div>Loading...</div>)
		}
		let data = state.venues
		let venueLinks = this.venueLinks(state)
		let artistLinks = this.artistLinks(state)
        let eventLinks = this.eventLinks(state)
		return (
			<div className="gradientAnim">
                <Row className="justify-content-center">
                    <Col sm="6" className="justify-content-center" xs="auto">
                        <Card id="Venues" class="text-center" border="0" body inverse style={{ backgroundColor: '#333', borderColor: '#333' }}>
                            <CardHeader tag="h2" style={{ textAlign: 'center' }}>{data.name}</CardHeader>
							<br></br>
                            <CardImg top width="100%" src={this.artistPic(state)} />
                            <br></br>
							<CardText tag="h5"> Address: {data.address + "\n"} </CardText>
                            <CardText tag="h5"> Phone #: {data.number + "\n"} </CardText>
                            <CardText tag="h5"> Google Rating: {data.rating + "\n"} </CardText>
                            <CardText tag="h5"> Venue Type: {data.type} </CardText>
                            <br></br>
							<ButtonToolbar>
								<Button color="light" href={venueLinks[0]} size='md' block> <b>{venueLinks[1]}'s</b> Website</Button>
								<Button color="light" href={artistLinks[0]} size='md' block> Featuring <b>{artistLinks[1]}</b></Button>
								<Button color="light" href={eventLinks[0]} size='md' block> Related Event: <b>{eventLinks[1]}</b></Button>
                            </ButtonToolbar>
							<br></br>
                            <MapContainer let lat = {data.latitude} let lng = {data.longitude}/>
							<br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
							<br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
						</Card>
					</Col>
				</Row>
			</div>

		)
	}
}
