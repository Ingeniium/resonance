﻿import React, { Component } from "react";
import Chart from "./Chart";
import './Visualizations.css';
import bg from '../components/seigaiha.png'

class Visualizations extends React.Component {

    render() {
        return (
            <div style={{ backgroundImage: 'url('+bg+')' }}>
                <br></br>
                <Chart size={[window.screen.width / 5 * 4, 500]} />
            </div>
        );
    }
}

export default Visualizations;
