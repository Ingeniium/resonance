﻿import React, { Component } from 'react';
import { scaleLinear, scaleBand } from 'd3-scale';
import { max } from 'd3-array';
import { select } from 'd3-selection';
import { axisBottom, axisLeft } from 'd3-axis';
import { format } from 'd3-format';
import * as d3 from "d3";

const venues = ["Alamodome", "House of Blues Dallas", "Revention Music Center", "Dickies Arena", "Wagner Noël Performing Arts Center", "Charline McCombs Empire Theatre", "",]

class Chart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            ratings: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            loading: true
        };
    }

    componentDidMount() {
        for (let i = 0; i < venues.length; i++) {
            fetch('http://flask-env.iy8jxgvnfb.us-west-2.elasticbeanstalk.com/api/venues')
                .then(response => response.json()).then(json => {
                    let data = json[i];
                    var val = ''
                    var q = Object.entries(data)
                        .map(([key, value]) => {
                            if (key == 'rating') {
                                this.state.ratings[i] = value
                            }
                            if (key == 'name') {
                                venues[i] = value
                            }
                        })
                    this.setState({ data: data, loading: false });
                })
        }
        if (!(this.state.ratings).includes(0)) {
            this.createBarChart();
        }
    }


    componentDidUpdate() {
        if (!(this.state.ratings).includes(0)) {
            this.createBarChart();
        }
    }
    createBarChart() {
        const margin = {
            top: 10,
            bottom: 30,
            left: 100,
            right: 50
        }

        let width = 1400;
        let height = 500;
        let x = d3.scaleBand()
            .domain(venues)
            .range([margin.left, width - margin.right])
            .padding(0.1);

        let y = d3.scaleLinear()
            .domain([0, d3.max(this.state.ratings)]).nice()
            .range([height - margin.bottom, margin.top]);

        let xAxis = g => g
            .attr("transform", `translate(0,${height - margin.bottom})`)
            .call(d3.axisBottom(x)
                .tickSizeOuter(0));

        let yAxis = g => g
            .attr("transform", `translate(${margin.left},0)`)
            .call(d3.axisLeft(y))
            .call(g => g.select(".domain").remove());


        const svg = d3.select("#body3");

        svg.append("g")
            .style("fill", "#69b3a2")
            .selectAll("rect").data(this.state.ratings).enter().append("rect")
            .attr("x", (d, i) => x(venues[i]))
            .attr("y", d => y(d))
            .attr("height", d => y(0) - y(d))
            .attr("width", x.bandwidth());

        svg.append("g")
            .call(xAxis)
            .append('text')
            .attr('x', (this.props.size[0]) / 2 + 150) 
            .attr('y', 45)
            .attr('fill', '#000')
            .style('font-size', '20px')
            .style('text-anchor', 'middle')
            .text('Venue');

        svg.append("g")
            .call(yAxis)
            .append('text')
            .attr('x', 0)
            .attr('y', 0)
            .attr('transform', 'translate(-' + (margin.left - 40) + `, ${this.props.size[1] / 2}) rotate(-90)`)
            .attr('fill', '#000')
            .style('font-size', '20px')
            .style('text-anchor', 'middle')
            .text('Ratings');
    }
    render() {
        if (this.state.loading) {
            return <p>Loading...</p>
        }
        return (
            <div align="center">
                <h1>Venue Ratings</h1>
                <svg id='body3'
                    width={1500} height={700}>
                </svg>
            </div>
        );
    }
}

export default Chart;

