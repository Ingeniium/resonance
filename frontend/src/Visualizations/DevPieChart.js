﻿import React, { Component } from 'react';
import * as d3 from "d3";

var crimes =
{
    Aggravated_Assault: 0,
    Arson: 0,
    Burglary: 0,
    Homicide: 0,
    Larceny: 0,
    Property_Crime: 0,
    Robbery: 0,
    Vehicle_Theft: 0,
    Violent_Crime: 0
}

class PieChart extends Component {


    constructor(props) {
        super(props);
        this.state = {
            data: [],
            list: {},
            loading: true
        };
    }

    componentDidMount() {
        fetch('http://dev.movecity.live/v1/api/Crime')
            .then(response => response.json()).then(json => {
                let data = json.objects;
                for (let i = 0; i < data.length; i++) {

                    crimes.Aggravated_Assault = crimes.Aggravated_Assault + parseInt(data[i]['Aggravated_Assault'])
                    crimes.Arson = crimes.Arson + parseInt(data[i]['Arson'])
                    crimes.Burglary = crimes.Burglary + parseInt(data[i]['Burglary'])
                    crimes.Homicide = crimes.Homicide + parseInt(data[i]['Homicide'])
                    crimes.Larceny = crimes.Larceny + parseInt(data[i]['Larceny'])
                    crimes.Property_Crime = crimes.Property_Crime + parseInt(data[i]['Property_Crime'])
                    crimes.Robbery = crimes.Robbery + parseInt(data[i]['Robbery'])
                    crimes.Vehicle_Theft = crimes.Vehicle_Theft + parseInt(data[i]['Vehicle_Theft'])
                    crimes.Violent_Crime = crimes.Violent_Crime + parseInt(data[i]['Violent_Crime'])
                }
                this.setState({ list: data });
            })
        this.state.loading = false
    }

    render() {

        var data = {
            Aggravated_Assault: crimes.Aggravated_Assault,
            Arson: crimes.Arson,
            Burglary: crimes.Burglary,
            Homicide: crimes.Homicide,
            Larceny: crimes.Larceny,
            Property_Crime: crimes.Property_Crime,
            Robbery: crimes.Robbery,
            Vehicle_Theft: crimes.Vehicle_Theft,
            Violent_Crime: crimes.Violent_Crime,
        }

        var width = 650
        var height = 650
        var margin = 40

        var radius = Math.min(width, height) / 2

        var svg = d3.select("#vis4")
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");


        var color = d3.scaleOrdinal()
            .domain(data)
            .range(["#ff0000", "#0000ff", "#00FF00", "#FF00FF", "#00FFFF", "#FF8C00"])

        var arcGenerator = d3.arc()
            .innerRadius(0)
            .outerRadius(radius)


        var pie = d3.pie()
            .value(function (d) { return d.value; })
        var data_ready = pie(d3.entries(data))

        svg
            .selectAll('vis4')
            .data(data_ready)
            .enter()
            .append('path')
            .attr('d', d3.arc()
                .innerRadius(0)
                .outerRadius(radius)
            )
            .attr('fill', function (d) { return (color(d.data.key)) })
            .attr("stroke", "black")
            .style("stroke-width", "2px")
            .style("opacity", 0.7)

        svg
            .selectAll('vis4')
            .data(data_ready)
            .enter()
            .append('text')
            .text(function (d) { return d.data.key })
            .attr("transform", function (d) { return "translate(" + arcGenerator.centroid(d) + ")"; })
            .style("text-anchor", "middle")
            .style("font-size", 17)




        return (
            <div align="center" style={{ textAlign: "center" }}>
                <h1>Division of Incidents by Crime Type</h1>
                <br></br>
                <svg id='vis4' width={650} height={800}></svg>
            </div>
        );

    }
}

export default PieChart;
