﻿import React, { Component } from 'react';
import { scaleLinear, scaleBand } from 'd3-scale';
import { max } from 'd3-array';
import { select } from 'd3-selection';
import { axisBottom, axisLeft } from 'd3-axis';
import { format } from 'd3-format';
import * as d3 from "d3";

const states = ["", "", "", "", "", "", "", "", "", ""]

class DevChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            cities: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            loading: true
        };
    }

    componentDidMount() {
        fetch('http://dev.movecity.live/v1/api/City')
            .then(response => response.json()).then(json => {
                let data = json.objects;
                var num = 1
                var name = ""
                var index = 0
                for (let i = 0; i < data.length; i++) {
                    if (i == 0) {
                        name = data[i]['State']
                    }
                    if (data[i]['State'] == name) {
                        num += 1
                    }
                    else {
                        this.state.cities[index] = num
                        num = 1
                        name = data[i]['State']
                        states[index] = data[i]['State']
                        index += 1
                    }
                }
                this.setState({ data: data });
            })
        this.state.loading = false
    }


    componentDidUpdate() {
        if (!(this.state.cities).includes(0)) {
            this.createBarChart();
        }
    }
    createBarChart() {
        let width = 1400;
        let height = 500;
        const margin = {
            top: 30,
            bottom: 30,
            left: 100,
            right: 50
        }
        let x = d3.scaleBand()
            .domain(states)
            .range([margin.left, width - margin.right])
            .padding(0.1);

        let y = d3.scaleLinear()
            .domain([0, d3.max(this.state.cities)]).nice()
            .range([height - margin.bottom, margin.top]);

        let xAxis = g => g
            .attr("transform", `translate(0,${height - margin.bottom})`)
            .call(d3.axisBottom(x)
                .tickSizeOuter(0));

        let yAxis = g => g
            .attr("transform", `translate(${margin.left},0)`)
            .call(d3.axisLeft(y))
            .call(g => g.select(".domain").remove());


        const svg = d3.select("#body4");

        svg.append("g")
            .style("fill", "#de8807")
            .selectAll("rect").data(this.state.cities).enter().append("rect")
            .attr("x", (d, i) => x(states[i]))
            .attr("y", d => y(d))
            .attr("height", d => y(0) - y(d))
            .attr("width", x.bandwidth());

        svg.append("g")
            .call(xAxis)
            .append('text')
            .attr('x', (this.props.size[0] + 300) / 2)
            .attr('y', 45)
            .attr('fill', '#000')
            .style('font-size', '25px')
            .style('text-anchor', 'middle')
            .text('States');
        ;

        svg.append("g")
            .call(yAxis)
            .append('text')
            .attr('x', 0)
            .attr('y', 0)
            .attr('transform', 'translate(-' + (margin.left - 40) + `, ${this.props.size[1] / 2}) rotate(-90)`)
            .attr('fill', '#000')
            .style('font-size', '20px')
            .style('text-anchor', 'middle')
            .text('Number of cities');

    }
    render() {
        if (this.state.loading) {
            return <p>Loading...</p>
        }
        return (
            <div align="center">
                <h1>Number of Cities Analyzed per State</h1>
                <svg id='body4'
                    width={1500} height={700}>
                </svg>
            </div>
        );
    }
}

export default DevChart;

