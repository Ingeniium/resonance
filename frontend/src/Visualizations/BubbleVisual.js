﻿import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Card, ListGroup, ListGroupItem } from 'react-bootstrap';
import BubbleChart from './BubbleChart';
import bg from '../components/seigaiha.png'

var list = [
    {
        label: "Eagles",
        value: 0
    }, {
        label: "Bad Bunny",
        value: 0
    }, {
        label: "Celine Dion: Courage World Tour",
        value: 0
    }, {
        label: "Tool",
        value: 0
    }, {
        label: "SEVENTEEN World Tour Ode To You",
        value: 0
    }, {
        label: "SuperM: We Are The Future Live",
        value: 0
    }, {
        label: "Trans-Siberian Orchestra 2019 Presented By Hallmark Channel",
        value: 0
    }, {
        label: "Marilyn Manson",
        value: 0
    }, {
        label: "The Chainsmokers/5 Seconds of Summer/Lennon Stella: World War Joy Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }, {
        label: "Ariana Grande: Sweetener World Tour",
        value: 0
    }
];

class Bubble extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: true
        };
    }

    componentDidMount() {
        fetch('http://flask-env.iy8jxgvnfb.us-west-2.elasticbeanstalk.com/api/artists')
            .then(response => response.json()).then(json => {
                let data = json;
                for (let i = 0; i < data.length; i++) {
                    
                        list[i].label = data[i]['name']
                    list[i].value = data[i]['popularity']
                }
                this.setState({ data: data });
            })
        this.state.loading = false
    }

    render() {
        if (this.state.loading) {
            return <p>Loading...</p>
        }
        return (
            <div align="center" style={{ backgroundImage: 'url('+bg+')' }}>
                <br></br>
                <h1>Popularity by Artists</h1>
                <BubbleChart
                    graph={{
                        zoom: 0.75,
                        offsetX: 0.25,
                        offsetY: 0.0,
                    }}
                    width={1200}
                    height={1000}
                    fontFamily="Arial"
                    data={list}
                />
            </div>
        )
    }
}
export default Bubble