﻿import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Card, ListGroup, ListGroupItem } from 'react-bootstrap';
import BubbleChart from './BubbleChart';
import bg from '../components/seigaiha.png'

var list = [
    {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, {
        label: "Eagles",
        value: 0
    }, 
];

class DevBubble extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: true
        };
    }

    componentDidMount() {
        fetch('http://dev.movecity.live/v1/api/Climate')
            .then(response => response.json()).then(json => {
                let data = json.objects;
                var cur = "";
                var index = 0
                for (let i = 0; i < data.length; i++) {
                    if (data[i]['State'] != cur) {
                        list[index].label = data[i]['State']
                        list[index].value = data[i]['RainDays']
                        cur = data[i]['State']
                        index += 1
                    }
                }
                this.setState({ data: data });
            })
        this.state.loading = false
    }

    render() {
        if (this.state.loading) {
            return <p>Loading...</p>
        }
        return (
            <div align="center" style={{ backgroundImage: 'url(' + bg + ')' }}>
                <br></br>
                <h1>Days Raining of each State</h1>
                <BubbleChart
                    graph={{
                        zoom: 0.75,
                        offsetX: 0.25,
                        offsetY: 0.0,
                    }}
                    width={1200}
                    height={1000}
                    fontFamily="Arial"
                    data={list}
                />
            </div>
        )
    }
}
export default DevBubble