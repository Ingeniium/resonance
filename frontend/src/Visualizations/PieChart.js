﻿import React, { Component } from 'react';
import * as d3 from "d3";

class PieChart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: true
        };
    }

    componentDidMount() {
        fetch('http://flask-env.iy8jxgvnfb.us-west-2.elasticbeanstalk.com/api/artists')
            .then(response => response.json()).then(json => {
                let data = json;
                this.setState({ data: data });
            })
        this.state.loading = false
    }

    render() {

        var data = {
            Pop: 11,
            Rock: 4,
            Metal: 6,
            Country: 5,
            Rap: 2,
            Other: 14,
        }

        var width = 450
        var height = 450
        var margin = 40

        var radius = Math.min(width, height) / 2

        var svg = d3.select("#vis3")
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");


        var color = d3.scaleOrdinal()
            .domain(data)
            .range(["#ff0000", "#0000ff", "#00FF00", "#FF00FF", "#00FFFF", "#FF8C00"])

        var arcGenerator = d3.arc()
            .innerRadius(0)
            .outerRadius(radius)


        var pie = d3.pie()
            .value(function (d) { return d.value; })
        var data_ready = pie(d3.entries(data))

        svg
            .selectAll('vis3')
            .data(data_ready)
            .enter()
            .append('path')
            .attr('d', d3.arc()
                .innerRadius(0)
                .outerRadius(radius)
            )
            .attr('fill', function (d) { return (color(d.data.key)) })
            .attr("stroke", "black")
            .style("stroke-width", "2px")
            .style("opacity", 0.7)

        svg
            .selectAll('vis3')
            .data(data_ready)
            .enter()
            .append('text')
            .text(function (d) { return d.data.key })
            .attr("transform", function (d) { return "translate(" + arcGenerator.centroid(d) + ")"; })
            .style("text-anchor", "middle")
            .style("font-size", 25)




        return (
            <div align="center" style={{ textAlign: "center" }}>
                <h1>Diversity of Genres for Artists</h1>
                <br></br>
                <svg id='vis3' width={450} height={800}></svg>
            </div>
        );

    }
}

export default PieChart;
