﻿import React, { Component } from "react";
import DevPieChart from "./DevPieChart";
import './Visualizations.css';
import bg from '../components/seigaiha.png'

class Visualizations extends React.Component {

    render() {
        return (
            <div style={{ backgroundImage: 'url(' + bg + ')' }}>
                <br></br>
                <DevPieChart />
            </div>
        );
    }
}

export default Visualizations;