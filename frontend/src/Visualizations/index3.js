﻿import React, { Component } from "react";
import PieChart from "./PieChart";
import './Visualizations.css';
import bg from '../components/seigaiha.png'

class Visualizations extends React.Component {

    render() {
        return (
            <div style={{ backgroundImage: 'url(' + bg + ')' }}>
                <br></br>
                <PieChart/>
            </div>
        );
    }
}

export default Visualizations;