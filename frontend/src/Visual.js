﻿
import React, { Component, Link } from 'react'
import BarChart from './BarChart'

const artists = ["", "", "", "", "", "", "", "", "", "", "", ""]

export default class Visual extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [], rankings: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            loading: true, width: 1500,
            height: 700,
        }
    }

    helper() {
        
    }

    componentDidMount() {
            fetch('http://flask-env.iy8jxgvnfb.us-west-2.elasticbeanstalk.com/api/artists')
                .then(response => response.json()).then(json => {
                    let data = json;
                    for (let i = 0; i < artists.length; i++) {
                        this.state.rankings[i] = data[i]['popularity']
                        artists[i] = data[i]['name']
                    }
                    this.setState({ data: data });
                })
        this.state.loading = false
    }

    render() {
        if (this.state.loading) {
            return (<div>Loading...</div>)
        }
        return (
            <div>
                <h1>Artist Popularity Rankings</h1>
                <BarChart
                    width={this.state.width}
                    height={this.state.height}
                    fontFamily="Arial"
                    data={this.state.rankings}
                    name={artists}
                />
                <p>{artists}</p>
            </div>
        )
    }

}