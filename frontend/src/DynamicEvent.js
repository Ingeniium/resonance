import React, { Component } from 'react'
import DynamicInstance from './DynamicInstance'
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, CardLink, Button, Row, Col, ButtonToolbar, Container, CardHeader
} from 'reactstrap';
import MapContainer from './map'
export default class DynamicEvent extends React.Component {

    constructor(props) {
        super(props)
    }


    async componentDidMount() {
        var url = this.props.domain + "/events/name/" + this.props.params
        await this.props.fetchData(url)
    }

	/* Using an array of size 2 gives us a nice way to
	simulate conditional rendering involving links to
	other pages;If there is no valid venue object associated
	with an artist, for example, no words are displayed,
	nor will a link to anything be present. If there is, both
	a sentence and a link to the appropriate page appear in tandem. */
    venueLinks(state) {
        let venue = state.venues
        var links = []
        links.concat(null)
        links.concat(null)
		if (venue && venue.name != null && venue.name != "") {
            links[0] = "/venues/" + venue.name
            links[1] = venue.name
        }
        return links
    }

    artistLinks(state) {
        let artist = state.artists
        var links = []
        links.concat(null)
        links.concat(null)
		if (artist && artist.name != null && artist.name != "") {
            links[0] = "/artists/" + artist.name
            links[1] = artist.name
        }
        return links
    }

    eventLinks(state) {
        let event = state.events
        var links = []
        links.concat(null)
        links.concat(null)
        if (event && event.url) {
            links[0] = event.url
            links[1] = "Ticketmaster Website"
        }
        return links
    }
    render() {
        let state = this.props.getState()
        if (!state.loaded) {
            return (<div>Loading...</div>)
        }
        else {
            let data = state.events
            let venueLinks = this.venueLinks(state)
            let artistLinks = this.artistLinks(state)
			let eventLinks = this.eventLinks(state)
			
			return (
				<div className="gradientAnim">
                    <Row className="justify-content-center">
                        <Col sm="6" className="justify-content-center" xs="auto" >
                            <Card id="Concerts" class="text-center" border="0" body inverse style={{ backgroundColor: '#333', borderColor: '#333' }}>
                                <CardHeader tag="h2" style={{ textAlign: 'center' }}>{data.name}</CardHeader>
                                <br></br>
                                <CardImg top width="100%" src={data.images} alt="Card image cap" />
                                <br></br>
                                <CardText tag="h5"> Date: {data.date} </CardText>
                                <CardText tag="h5"> Time:  </CardText>
                                <CardText tag="h6"> {data.time} </CardText>
                                <CardText tag="h5"> Price:  </CardText>
                                <CardText tag="h6"> $ {data.price} </CardText>
                                <CardText tag="h5"> Genre:  </CardText>
                                <CardText tag="h6"> {data.genre} </CardText>
                                <br></br>
                                <ButtonToolbar>
                                <Button color="light" href={eventLinks[0]} size='md' block> <b>{eventLinks[1]}</b></Button>
                                    <Button color="light" href={venueLinks[0]} size='md' block> Event Location: <b>{venueLinks[1]}</b></Button>
                                    <Button color="light" href={artistLinks[0]} size='md' block> Featured Artist: <b>{artistLinks[1]}</b></Button>
                                </ButtonToolbar>
                                <br></br>
                                <MapContainer let lat = {data.latitude} let lng = {data.longitude}/>
                                <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                                <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                            </Card>
                        </Col>
                    </Row>
                </div>
            )
        }
    }
}
