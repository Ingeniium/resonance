
import React, { Component, Link } from 'react'
import ReactTable from 'react-table'
import Highlighter from 'react-highlight-words'
import bg from './components/seigaiha.png'

const domain = "http://flask-env.iy8jxgvnfb.us-west-2.elasticbeanstalk.com/api/all"

export default class SearchResult extends Component{
	
	constructor(props) {
		super(props)
		this.state = {
			data: [], value: "", loading: true,
			columns:
			[
				{
					Header: "Name",
					accessor: "name",
					Cell: (rows, props) => {
						var tokens = rows.value.split("@*")

						return (
							//Note that a relative url for href was used
							<a href={tokens[1] + "/" + tokens[0]}>
								<Highlighter
									searchWords={[this.state.value]}
									textToHighlight={tokens[0]}
								/>
							</a>
							)
					}
				}
			]
		}
	}

	filterMethod(filter, rows) {
		return rows.value.includes("C")
	}

	async componentDidMount() {
		fetch(domain)
			.then(response => response.json())
			.then(json => this.setState({ data: json, loading : false }))
	}

	onValueChange(event) {
		this.setState({ value: event.target.value, loading : false })
	}

	render() {
		if (this.state.loading) {
			return (<div style={{ color: 'black' }}>Loading...</div>)
		}
		var filtered = this.state.data.filter((instance) => {
			return String(instance.name).toLowerCase().includes(this.state.value.toLowerCase())
		})
		/*The characters after the @* help set the correct links */
		for (var instance of filtered) {
			instance.name += "@*"
			if (instance.venue != null && instance.event != null) 
				instance.name += "artists"
			else if (instance.venue != null && instance.artist != null) 
				instance.name += "events"
			else
				instance.name += "venues"
		}
		return (
			<div>
				<input type="text"
					onChange={this.onValueChange.bind(this)}
					placeholder="Enter a keyword..."
					size="70"/>
				<br></br>
				<br></br>
				<ReactTable style={{ backgroundImage: 'url('+bg+')' }}
					data={filtered}
					columns={this.state.columns}
					defaultPageSize={10}
					pageSizeOptions={[10, 20]}
				/>
			</div>
		)
	}

}