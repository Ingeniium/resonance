import React, { Component } from 'react'
import {
    Card, CardImg, CardText, CardBody, CardHeader,
    CardTitle, CardSubtitle, CardLink, Button, Row, Col, ButtonToolbar, ListGroup, ListGroupItem
} from 'reactstrap';
import Youtube from './youtube'

export default class DynamicArtist extends React.Component {

    constructor(props) {
        super(props)
    }


    async componentDidMount() {
        var url = this.props.domain + "/events/artist/" + this.props.params
        await this.props.fetchData(url)
    }

	/* Using an array of size 2 gives us a nice way to
	simulate conditional rendering involving links to
	other pages;If there is no valid venue object associated
	with an artist, for example, no words are displayed,
	nor will a link to anything be present. If there is, both
	a sentence and a link to the appropriate page appear in tandem. */
    venueLinks(state) {
        let venue = state.venues
        var links = []
        links.concat(null)
        links.concat(null)
        if (venue && venue.name != null && venue.name != "") {
            links[0] = "/venues/" + venue.name
            links[1] = venue.name
        }
        return links
    }

    artistLinks(state) {
        let artist = state.artists
        var links = []
        links.concat(null)
        links.concat(null)
        if (artist.spotifyurl && artist.spotifyurl != "") {
            links[0] = artist.spotifyurl
            links[1] = "Spotify Artist Page"
        }
        return links
    }

    genreLinks(state) {
        let artist = state.artists
        let genres = artist.genre
        var links = []
        links = genres
        return links
    }

    eventLinks(state) {
        let event = state.events
        var links = []
        links.concat(null)
        links.concat(null)
        if (event && event.name && event.name != "") {
            links[0] = "/events/" + event.name
            links[1] = event.name
        }
        return links
    }

    render() {
        let state = this.props.getState();
        if (!state.loaded) {
            return (<div>Loading...</div>)
        }
        else {
            let data = state.artists
            let venueLinks = this.venueLinks(state)
            let artistLinks = this.artistLinks(state)
            let eventLinks = this.eventLinks(state)
			return (
				<div className="gradientAnim" >
                    <Row className="justify-content-center">
                        <Col sm="6" className="justify-content-center" xs="auto">
                            <Card id="Artists" class="text-center" border="0" body inverse style={{ backgroundColor: '#333', borderColor: '#333' }}>
                                <CardHeader tag="h2" style={{ textAlign: 'center' }}>{data.name}</CardHeader>
                                <br></br>
                                <CardImg top width="100%" src={data.image} alt="Card image cap" />
                                <br></br>
                                <CardText tag="h5"> Popularity: {data.popularity} out of 100 </CardText>
                                <CardText tag="h5"> Genres:  </CardText>
                                <CardText tag="h6"> {data.genre.replace('[', '').replace(']', '').split(',').map((addr, idx) => (<span key={idx}>{addr}<br /></span>))} </CardText>
                                <CardText tag="h5"> Albums:  </CardText>
                                <CardText tag="h6"> {data.albums.split(',').map((addr, idx) => (<span key={idx}>{addr}<br /></span>))} </CardText>
                                <CardText tag="h5"> Top Songs:  </CardText>
                                <CardText tag="h6"> {data.top_songs.split(',').map((addr, idx) => (<span key={idx}>{addr}<br /></span>))} </CardText>
                                <br></br>
                                <ButtonToolbar>
                                    <Button color="light" href={artistLinks[0]} size='md' block> <b>{artistLinks[1]}</b></Button>
                                    <Button color="light" href={eventLinks[0]} size='md' block> Upcoming Event: <b>{eventLinks[1]}</b></Button>
                                    <Button color="light" href={venueLinks[0]} size='md' block> Event Location: <b>{venueLinks[1]}</b></Button>
                                </ButtonToolbar>
                                <br></br>
                                <CardText tag="h5" />
                                <iframe width="560" height="315" src={data.youtube} frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </Card>
                        </Col>
                    </Row>
                </div>

            )
        }
    }
}
