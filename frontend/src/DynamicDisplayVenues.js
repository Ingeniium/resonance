import React from 'react'
import ReactTable from 'react-table'
import Highlighter from 'react-highlight-words'
import bg from './components/seigaiha.png'

const domain = "http://flask-env.iy8jxgvnfb.us-west-2.elasticbeanstalk.com/api/venues"

export default class DisplayVenues extends React.Component {

    constructor(props) {
        super(props)
        let match = this.props.match
		this.state = {
			data: [], loading: true, minDays: 0, minRating: 0,
			nameKey : "",
			columns  : [
				{
					Header: "Venue Name",
					accessor: "name",
					minWidth: 300,
					Cell: (rows) => <a href={"/venues/" + rows.value}>{
						<Highlighter
							searchWords={[this.state.nameKey]}
							textToHighlight={rows.value}
						/>}</a>,

					Filter: (filter, onChange) =>
						<input type="text"
							onChange={(event) => this.setState({
								nameKey: event.target.value
							})}
							size="30"
						/>
				},
				{
					Header: "Location",
					accessor: "image",
					Cell: (rows) => <a href={rows.value}>Google Maps</a>,
					minWidth: 120,
					filterable: false
				},
				{
					Header: "Rating",
					accessor: "rating",
					Filter: (filter, onChange) =>
						<input type="number"
							onChange={(event) => this.setState({
								minRating: event.target.value
							})}
							placeholder="Minimum Rating" />,
					width: 130
				},
				{
					Header: "Address",
					accessor: "address",
					minWidth: 300
				},
				{
					Header: "# of Days Open",
					accessor: "hours",
					Cell: (rows) => {
						if (!rows.value || rows.value == "None")
							return "N/A"
						var closed = rows.value.match(/Closed/g)
						closed = closed ? closed.length : 0
						return 7 - closed
					},
					sortMethod: (lhs, rhs) =>
					{
						if (lhs == "None") 
							return -1
						if (rhs == "None")
							return 1
						var closedl = lhs.match(/Closed/g)
						closedl = closedl ? closedl.length : 0

						var closedr = rhs.match(/Closed/g)
						closedr = closedr ? closedr.length : 0

						return closedl < closedr ? 1 : -1
					},
					Filter: (filter, onChange) =>
						<input type="number"
							onChange={(event) => this.setState({
								minDays: event.target.value
							})}
							placeholder="0" />,
					width : 100
				},
				{
					Header: "Venue Type",
					accessor: "type",
					filterMethod: (filter, row) => {
						if (filter.value === "all")
							return true;
						return row[filter.id] && row[filter.id] == filter.value
					},
					Filter: ({ filter, onChange }) => 
					<select
							onChange= { event => onChange(event.target.value) }
							style= {{ width: "100%" }}
					value = { filter? filter.value : "all"}
					>
					<option value="all">Show All</option>
					<option value="point_of_interest">Point of Interest</option>
					<option value="tourist_attraction">Tourist Attraction</option>
					<option value="stadium">Stadium</option>
					<option value="university">University</option>
					<option value="movie_theater">Movie Theater</option>
					<option value="night_club">Night Club</option>
					<option value="bar">Bar</option>
					<option value="park">Park</option>
						</select >,
					width: 130
				},
				{
					Header: "Phone Number",
					accessor: "number",
					Cell: (rows) => {
						if (!rows.value) { return "N/A" }
						else { return rows.value }
					},
					filterMethod: (filter, row) => {
						if (filter.value === "all")
							return true
						return (!row[filter.id] && filter.value == "false")
							|| (row[filter.id] && filter.value == "true")
					},
					Filter: ({ filter, onChange }) =>
					<select
						onChange={event => onChange(event.target.value)}
						style={{ width: "100%" }}
						value={filter ? filter.value : "all"}
					>
						<option value="all">Show All</option>
						<option value="true">Has Phone #</option>
						<option value="false">No Phone #</option>
						</select >,
					minWidth: 130
				},
				{
					Header: "Website",
					accessor: "website",
					Cell: (rows) => {
						if (!rows.value) { return "N/A" }
						else { return <a href={rows.value}>{rows.value}</a> }
					},
					filterMethod: (filter, row) => {
						if (filter.value === "all")
							return true
						return (!row[filter.id] && filter.value == "false")
							|| (row[filter.id] && filter.value == "true")
					},
					Filter: ({ filter, onChange }) =>
						<select
							onChange={event => onChange(event.target.value)}
							style={{ width: "100%" }}
							value={filter ? filter.value : "all"}
						>
							<option value="all">Show All</option>
							<option value="true">Has External Website</option>
							<option value="false">No External Website</option>
						</select >,
					minWidth : 300
				}
			]
		}
    }

    componentDidMount() {
        fetch(domain)
            .then(response => response.json())
            .then(obj => this.setState({ data: obj, loading: false }))
    }

	render() {
		if (this.state.loading) {
			return (<div>Loading Venues...</div>)
		}
		/* Manual filter for the input filters(min and max)
		filters.*/
		var filtered = this.state.data.filter((instance) => {
			/*Check if this venue has characters that are
			currently being searched for.*/
			if ((!instance.name || instance.name == "") &&
				this.state.nameKey != "")
				return false
			else if (!instance.name.toLowerCase().includes(
				this.state.nameKey.toLowerCase()))
				return false
			/* Filter out venue is open for less than state.minDays
			   days, where state.minDays is user inputted. */
			if ((!instance.hours || instance.hours == "None")
				&& this.state.minDays > 0)
				return false
			var closed = instance.hours.match(/Closed/g)
			closed = closed ? closed.length : 0
			if (7 - closed < this.state.minDays)
				return false
			/* Filter out venues that have less than the
			   user inputted minimum rating. */
			if (!instance.rating && this.state.minRating > 0)
				return false
			if (instance.rating < this.state.minRating)
				return false 

			return true
		})
		return (
			<div>
				<ReactTable
					filterable
					data={filtered}
					columns={this.state.columns}
					defaultPageSize={10}
					pageSizeOptions={[10, 20]}
					className="-striped"
					style={{ backgroundImage: 'url('+bg+')', height: '100vh' }}
				/>
			</div>
		)
	}
}

