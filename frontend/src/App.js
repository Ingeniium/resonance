import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import logo from './components/Home/logo.png'
import Home from './Home';
import About from './About';
import DisplayVenues from './DynamicDisplayVenues';
import DisplayArtists from './DynamicDisplayArtists'
import DisplayEvents from './DynamicDisplayEvents'
import DynamicInstance from './DynamicInstance'
import { Nav, Navbar, NavDropdown } from "react-bootstrap";
import Visual1 from './Visualizations/index';
import Visual2 from './Visualizations/index3';
import Visual3 from './Visualizations/BubbleVisual';
import Visual4 from './Visualizations/index2';
import Visual5 from './Visualizations/devindex3';
import Visual6 from './Visualizations/DevBubbleVisual';

class App extends React.Component {
    render() {
        return (
            <Router>
                <Navbar bg="dark" variant="dark" collapseOnSelect expand="lg" navbar-static-top style={{ textTransform: 'uppercase' }} >
                    <Navbar.Brand href="/"><img src={logo} width="50px" alt="logo" style={{ paddingRight: '10px' }}/><b>Resonance</b></Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Item>
                                <Nav.Link as={Link} to="/about">About</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link as={Link} to="/artists">Artists</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link as={Link} to="/events">Events</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link as={Link} to="/venues">Venues</Nav.Link>
                            </Nav.Item>
                            <NavDropdown title="Visualizations" id="basic-nav-dropdown">
                                <NavDropdown.Header>Resonance</NavDropdown.Header>
                                <NavDropdown.Item href="/visualization1">Visualization 1</NavDropdown.Item>
                                <NavDropdown.Item href="/visualization2">Visualization 2</NavDropdown.Item>
                                <NavDropdown.Item href="/visualization3">Visualization 3</NavDropdown.Item>
                                <NavDropdown.Divider></NavDropdown.Divider>
                                <NavDropdown.Header>Move City</NavDropdown.Header>
                                <NavDropdown.Item href="/visualization4">Visualization 4</NavDropdown.Item>
                                <NavDropdown.Item href="/visualization5">Visualization 5</NavDropdown.Item>
                                <NavDropdown.Item href="/visualization6">Visualization 6</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route path='/about' component={About} />
                    <Route path="/artists/:artist" render={(props) => <DynamicInstance {...props} type="Artist" />}/>
					<Route path="/venues/:venue" render={(props) => <DynamicInstance {...props} type="Venue" />}/>
					<Route path="/events/:event" render={(props) => <DynamicInstance {...props} type="Event" /> }/>
					<Route exact path="/artists" component={DisplayArtists} />
					<Route exact path="/venues" component={DisplayVenues} />
                    <Route exact path="/events" component={DisplayEvents} />
                    <Route exact path="/visualization1" component={Visual1} />
                    <Route exact path="/visualization2" component={Visual2} />
                    <Route exact path="/visualization3" component={Visual3} />
                    <Route exact path="/visualization4" component={Visual4} />
                    <Route exact path="/visualization5" component={Visual5} />
                    <Route exact path="/visualization6" component={Visual6} /> 
                </Switch>
            </Router>
        );
    }
}

export default App;