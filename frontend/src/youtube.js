import React, {Component} from 'react';

class Youtube extends Component {
    render() {
        return(
           <div>
               /*ideally create a variable url that can be passed in
                from the database, and replace src="https://www.youtube.com/embed/nXJIVAy7kCM"
                with src=url (url being set to that string)
                */
               
                <iframe width="560" height="315" src="https://www.youtube.com/embed/PT2_F-1esPk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
           </div>
        );
    }
}

export default Youtube;
