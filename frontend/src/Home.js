import React from 'react'
import { Carousel, Button } from 'react-bootstrap'
import Search from './Search'
import bg from './components/seigaiha.png'
import Visual from './Visual';

class Home extends React.Component {
    render() {
        return (
            <Carousel style={{ backgroundImage: 'url('+bg+')' }}>
                <Carousel.Item>
                    <img className="vh-100 w-100" src="https://www.imore.com/sites/imore.com/files/styles/xlarge/public/field/image/2018/11/concert-goers-hero-image.jpg?itok=gkcEI1PL" alt="slide"></img>
                    <Carousel.Caption style={{ top: '30vh', bottom: 'auto'}}>
                        <h1 style={{ fontSize: '100px', textShadow: '1px 1px 5px black' }}>RESONANCE</h1>
                        <br></br>
                        <h5 style={{ fontSize: '30px', textShadow: '1px 1px 5px black' }}>Bringing people together through music</h5>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img className="vh-100 w-100" src="https://cdn.mos.cms.futurecdn.net/38WNhVt7qradw6zKgzCvfE.jpg" alt="slide"></img>
                    <Carousel.Caption style={{ top: '30vh', bottom: 'auto' }}>
                        <h1 style={{ fontSize: '50px', textShadow: '1px 1px 5px black' }}>EXPLORE ARTISTS</h1>
                        <br></br>
                        <Button variant="dark" href="/Artists" style={{ fontSize: '20px' }}>CLICK HERE</Button>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img className="vh-100 w-100" src="https://miro.medium.com/max/5576/1*bbrdaWB-Yz8MVm6T-QZuug.jpeg" alt="slide"></img>
                    <Carousel.Caption style={{ top: '30vh', bottom: 'auto' }}>
                        <h1 style={{ fontSize: '50px', textShadow: '1px 1px 5px black' }}>EXPLORE EVENTS</h1>
                        <br></br>
                        <Button variant="dark" href="/Events" style={{ fontSize: '20px' }}>CLICK HERE</Button>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img className="vh-100 w-100" src="https://www.godsavethepoints.com/wp-content/uploads/2018/06/47035636_m.jpg" alt="slide"></img>
                    <Carousel.Caption style={{ top: '30vh', bottom: 'auto' }}>
                        <h1 style={{ fontSize: '50px', textShadow: '1px 1px 5px black' }}>EXPLORE VENUES</h1>
                        <br></br>
                        <Button variant="dark" href="/Venues" style={{ fontSize: '20px' }}>CLICK HERE</Button>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img className="vh-100 w-100" src="https://article.images.consumerreports.org/f_auto/prod/content/dam/CRO%20Images%202019/Health/08August/Cr-Health-InlineHero-How-To-Hear-Better-At-Concerts-8-19" alt="slide"></img>
                    <Carousel.Caption style={{ top: '1vh', bottom: 'auto'}}>
                        <h1 style={{ fontSize: '50px', textShadow: '1px 1px 5px black' }}>SEARCH</h1>
                        <br></br>
                        <Search/>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        )
    }
}

export default Home;