import React, { Component } from 'react'
import {
	Card, CardImg, CardText, CardBody,
	CardTitle, CardSubtitle, CardLink, Button, Row, Col
} from 'reactstrap';
import DynamicEvent from  './DynamicEvent'
import DynamicVenue from './DynamicVenue'
import DynamicArtist from './DynamicArtist'
const domain = "http://flask-env.iy8jxgvnfb.us-west-2.elasticbeanstalk.com/api"

export default class DynamicInstance extends React.Component {
	constructor(props) {
		super(props)
		this.state = { loaded: false, idevent: null, events: null, artists: null, venues: null }

		let match = this.props.match
	}

	async fetchData(url) {
		/* First, get the event data for the instance based on
		the url.*/
		await fetch(url)
			.then(response => response.json())
			.then(event => this.setState({ events: event[0], artists: event[0].artist, venues: event[0].venue }))
			.catch(console.log("Error getting event data!"))
		/*Get the data of which artist(s) is playing at the event.
		  Try-catch error block displays error as well as allow
		  data to be retireved if it is missing other associated
		  instances(EX, if an event doesn't have an associated venue) */
		if (this.state.artists) {
			try {
				var url = domain + "/artists/name/" + this.state.events.artist
				console.log("Getting artist data... : " + url)
				await fetch(url)
					.then(response => response.json())
					.then(artist => this.setState({ artists: artist[0] }))
					.catch(console.log("Error getting artist data!"))
			}
			catch (e) { console.error("Error while getting artist data : " + e)}
		}
		/* Get the venue data of where the event is taking place */
		if (this.state.venues) {
			try {
				var url = domain + "/venues/name/" + this.state.events.venue
				console.log("Getting venue data... : " + url)
				await fetch(url)
					.then(response => response.json())
					.then(venue => this.setState({ venues: venue[0] }))
					.catch(console.log("Error getting venue data!"))
			}
			catch (e) { console.error("Error getting venue data : " + e) }
		}
		/* Change state.loaded to true so that the page will be rerendered
		   after getting instance data. */
		this.setState({ loaded: true })
	}

	/* Handler that allows child components to access
	state. */
	getState() {
		return this.state
	}

	/* Type of page rendered is based on the type sent in
	   from the react router. */
	render() {
		if (this.props.type == "Event") {
			return (<DynamicEvent params={this.props.match.params.event}
				domain={domain}
				fetchData={this.fetchData.bind(this)}
				getState={this.getState.bind(this)} />)
		}
		else if (this.props.type == "Artist") {
			return (<DynamicArtist params={this.props.match.params.artist}
				domain={domain}
				fetchData={this.fetchData.bind(this)}
				getState={this.getState.bind(this)} />)
		}
		else {
			return (<DynamicVenue params={this.props.match.params.venue}
				domain={domain}
				fetchData={this.fetchData.bind(this)}
				getState={this.getState.bind(this)} />)
		}

	}
	
	
}