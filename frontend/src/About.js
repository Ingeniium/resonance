import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Card, ListGroup, ListGroupItem } from 'react-bootstrap';
import andrew from './components/About/andrew.JPG'
import anwesh from './components/About/anwesh.jpg'
import kai from './components/About/kai.JPG'
import mahir from './components/About/mahir.jpg'
import william from './components/About/william.JPG'
import bg from './components/seigaiha.png'

var members = {
    "Andrew": {
        name: "DESKTOP-BJJBN02\\andre",
        pic: andrew,
        commits: 0,
        issues: 0,
        unitTests: 10,
        username: "Ingeniium",
        role: "Frontend",
        bio: "I leave the top button undone because I like to live life on the edge"
    },
    "Anwesh": {
        name: "Anwesh Basu",
        pic: anwesh,
        commits: 0,
        issues: 0,
        unitTests: 10,
        username: "AnweshBasu",
        role: "Backend",
        bio: "Turns out this picture of me is actually a painting. Gottem"
    },
    "Kai": {
        name: "Kai Lee",
        pic: kai,
        commits: 0,
        issues: 0,
        unitTests: 20,
        username: "kai_lee",
        role: "Frontend",
        bio: "I used to be Haesong but now I'm Kai ¯\\_(ツ)_/¯"
    },
    "Mahir": {
        name: "Mahir Karim",
        pic: mahir,
        commits: 0,
        issues: 0,
        unitTests: 14,
        username: "mahirkarim",
        role: "Backend",
        bio: "I'm a government double major, but we all know computers rule the world"
    },
    "William": {
        name: "William Wang",
        pic: william,
        commits: 0,
        issues: 0,
        unitTests: 0,
        username: "williamtzw",
        role: "Frontend/Devops",
        bio: "You can blame me for all the cringey captions on this page"
    }
};
var totals = {
    commits: 0,
    issues: 0,
    unitTests: 54
};

class About extends React.Component {
    componentDidMount() {
        Object.keys(members).forEach(member => {
            const name = members[member].name;
            fetch("https://gitlab.com/api/v4/projects/14503197/repository/contributors")
                .then(res => res.json())
                .then(res => {
                    for (let i = 0; i < res.length; i++) {
                        const currUser = res[i].name;
                        if (name == currUser) {
                            const currCommits = res[i].commits;
                            members[member].commits += currCommits;
                            totals.commits += currCommits;
                            const state = {}
                            state.commits = members[member].commits;
                            this.setState(state);
                        }
                    }
                })
        });
        Object.keys(members).forEach(member => {
            const username = members[member].username;
            fetch("https://gitlab.com/api/v4/projects/14503197/issues_statistics?assignee_username=" + username)
                .then(res => res.json())
                .then(res => {
                    const currIssues = res.statistics.counts.all;
                    members[member].issues = currIssues;
                    totals.issues += currIssues;
                    const state = {}
                    state.issues = members[member].issues;
                    this.setState(state);
                })
        });
    }

    render() {
        return (
            <div className="About" style={{ backgroundImage: 'url('+bg+')' }} >
                <Container>
                    <br></br><br></br>
                    <h1 style={{ fontSize: '50px', textAlign: "center" }}>ABOUT US</h1>
                    <h5 style={{ fontSize: '20px', textAlign: "center", margin: "25px" }}>Resonance is a website that connects users to concerts in their area performed by their favorite artists.<br></br>Here are its developers.</h5>
                    <br></br>
                    <Row>
                        <Col className="justify-content-center" md="12" lg="4">
                            <Card class="text-center" border="0" style={{ width: '300px' }}>
                                <Card.Img variant="top" src={members['Andrew'].pic} style={{ width: "100%", height: "20vw", objectFit: "cover" }} />
                                <Card.Body style={{textAlign: 'center'}}>
                                    <Card.Title>Andrew Olumbe</Card.Title>
                                    <Card.Subtitle>{members['Andrew'].role}</Card.Subtitle>
                                    <Card.Text></Card.Text>
                                    <Card.Text>{members['Andrew'].bio}</Card.Text>
                                    <ListGroup className="list-group-flush">
                                        <ListGroupItem><b>Commits: </b>{members['Andrew'].commits}</ListGroupItem>
                                        <ListGroupItem><b>Issues: </b>{members['Andrew'].issues}</ListGroupItem>
                                        <ListGroupItem><b>Unit Tests: </b>{members['Andrew'].unitTests}</ListGroupItem>
                                    </ListGroup>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col className="justify-content-center" md="12" lg="4">
                            <Card class="text-center" border="0" style={{ width: '300px' }}>
                                <Card.Img variant="top" src={members['Anwesh'].pic} style={{ width: "100%", height: "20vw", objectFit: "cover" }} />
                                <Card.Body style={{textAlign: 'center'}}>
                                    <Card.Title>Anwesh Basu</Card.Title>
                                    <Card.Subtitle>{members['Anwesh'].role}</Card.Subtitle>
                                    <Card.Text></Card.Text>
                                    <Card.Text>{members['Anwesh'].bio}</Card.Text>
                                    <ListGroup className="list-group-flush">
                                        <ListGroupItem><b>Commits: </b>{members['Anwesh'].commits}</ListGroupItem>
                                        <ListGroupItem><b>Issues: </b>{members['Anwesh'].issues}</ListGroupItem>
                                        <ListGroupItem><b>Unit Tests: </b>{members['Anwesh'].unitTests}</ListGroupItem>
                                    </ListGroup>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col className="justify-content-center" md="12" lg="4">
                            <Card class="text-center" border="0" style={{ width: '300px' }}>
                                <Card.Img variant="top" src={members['Kai'].pic} style={{ width: "100%", height: "20vw", objectFit: "cover" }} />
                                <Card.Body style={{textAlign: 'center'}}>
                                    <Card.Title>Kai Lee</Card.Title>
                                    <Card.Subtitle>{members['Kai'].role}</Card.Subtitle>
                                    <Card.Text></Card.Text>
                                    <Card.Text>{members['Kai'].bio}</Card.Text>
                                    <ListGroup className="list-group-flush">
                                        <ListGroupItem><b>Commits: </b>{members['Kai'].commits}</ListGroupItem>
                                        <ListGroupItem><b>Issues: </b>{members['Kai'].issues}</ListGroupItem>
                                        <ListGroupItem><b>Unit Tests: </b>{members['Kai'].unitTests}</ListGroupItem>
                                    </ListGroup>
                                </Card.Body>
                            </Card>
                        </Col>
                        </Row>
                        <br></br><br></br>
                        <Row>
                        <Col className="justify-content-center" md="12" lg="4">
                            <Card class="text-center" border="0" style={{ width: '300px' }}>
                                <Card.Img variant="top" src={members['Mahir'].pic} style={{ width: "100%", height: "20vw", objectFit: "cover" }} />
                                <Card.Body style={{textAlign: 'center'}}>
                                    <Card.Title>Mahir Karim</Card.Title>
                                    <Card.Subtitle>{members['Mahir'].role}</Card.Subtitle>
                                    <Card.Text></Card.Text>
                                    <Card.Text>{members['Mahir'].bio}</Card.Text>
                                    <ListGroup className="list-group-flush">
                                        <ListGroupItem><b>Commits: </b>{members['Mahir'].commits}</ListGroupItem>
                                        <ListGroupItem><b>Issues: </b>{members['Mahir'].issues}</ListGroupItem>
                                        <ListGroupItem><b>Unit Tests: </b>{members['Mahir'].unitTests}</ListGroupItem>
                                    </ListGroup>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col className="justify-content-center" md="12" lg="4">
                            <Card class="text-center" border="0" style={{ width: '300px' }}>
                                <Card.Img variant="top" src={members['William'].pic} style={{ width: "100%", height: "20vw", objectFit: "cover" }} />
                                <Card.Body style={{textAlign: 'center'}}>
                                    <Card.Title>William Wang</Card.Title>
                                    <Card.Subtitle>{members['William'].role}</Card.Subtitle>
                                    <Card.Text></Card.Text>
                                    <Card.Text>{members['William'].bio}</Card.Text>
                                    <ListGroup className="list-group-flush">
                                        <ListGroupItem><b>Commits: </b>{members['William'].commits}</ListGroupItem>
                                        <ListGroupItem><b>Issues: </b>{members['William'].issues}</ListGroupItem>
                                        <ListGroupItem><b>Unit Tests: </b>{members['William'].unitTests}</ListGroupItem>
                                    </ListGroup>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col className="justify-content-center" md="12" lg="4">
                            <Card class="text-center" border="0" style={{ width: '300px' }}>
                                <Card.Body style={{textAlign: 'center'}}>
                                    <br></br>
                                    <Card.Title>Project Information<br></br>&<br></br>Statistics</Card.Title>
                                    <br></br>
                                    <ListGroup className="list-group-flush">
                                        <ListGroupItem>
                                            <img src="https://about.gitlab.com/images/press/logo/png/gitlab-logo-gray-stacked-rgb.png" width="60px" alt="logo" style={{ paddingRight: '10px'}}/>
                                            <a href="https://gitlab.com/Ingeniium/resonance"><b>Gitlab Repo</b></a>
                                        </ListGroupItem>
                                        <ListGroupItem>
                                            <img src="https://assets.getpostman.com/common-share/postman-logo-stacked.svg" width="70px" alt="logo" style={{ paddingRight: '20px'}}/>
                                            <a href="https://documenter.getpostman.com/view/8976145/SVtR1VG4?version=latest"><b>Postman API</b></a>
                                        </ListGroupItem>
                                        <ListGroupItem><b>Total Commits: </b>{totals.commits}</ListGroupItem>
                                        <ListGroupItem><b>Total Issues: </b>{totals.issues}</ListGroupItem>
                                        <ListGroupItem><b>Total Unit Tests: </b>{totals.unitTests}</ListGroupItem>
                                    </ListGroup>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
                <br></br>
                <Container>
                    <h2 style={{ textAlign: "center", margin: "25px" }}>Data Sources</h2>
                    <ListGroup className="list-group-flush">
                        <ListGroupItem style={{ borderTop: '0' }}>
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/GoogleMaps_logo.svg/1024px-GoogleMaps_logo.svg.png" width="70px" alt="logo" style={{ float: 'left', padding: '10px', paddingRight: '20px' }}/>
                            <a href="https://developers.google.com/maps/documentation"><b>Google Maps API</b></a>
                            <p>We used the Google Maps API to fetch data on venues and concert locations</p>
                        </ListGroupItem>
                        <ListGroupItem>
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Spotify_logo_without_text.svg/168px-Spotify_logo_without_text.svg.png" width="70px" alt="logo" style={{ float: 'left', padding: '10px', paddingRight: '20px' }}/>
                            <a href="https://developer.spotify.com/documentation/web-api/"><b>Spotify API</b></a>
                            <p>We used the Spotify API to fetch data on artists</p>
                        </ListGroupItem>
                        <ListGroupItem>
                        <img src="https://media.licdn.com/dms/image/C560BAQHwOxo5hugMHw/company-logo_200_200/0?e=2159024400&v=beta&t=Hid8_zr3LsDImk1DXsJXpeyLQVQ2vhHkvukSqazQIKM" width="70px" alt="logo" style={{ float: 'left', padding: '10px', paddingRight: '20px' }}/>
                            <a href="https://developer.ticketmaster.com/products-and-docs/apis/getting-started/"><b>Ticketmaster API</b></a>
                            <p>We used the Ticketmaster API to fetch data on concerts</p>
                        </ListGroupItem>
                    </ListGroup>
                </Container>
                <br></br>
                <Container>
                    <h2 style={{ textAlign: "center", margin: "25px" }}>Tools</h2>
                    <ListGroup className="list-group-flush">
                        <ListGroupItem style={{ borderTop: '0' }}>
                            <img src="https://pronto-core-cdn.prontomarketing.com/2/wp-content/uploads/sites/1614/2019/07/21743298_1406722539365107_4308832733562613967_n.png" width="90px" alt="logo" style={{ float: 'left', padding: '10px', paddingRight: '20px' }}/>
                            <a href="https://aws.amazon.com"><b>Amazon Web Services</b></a>
                            <p>We used various AWS services like Route 53 to host our domain, CloudFront to make our site https compliant, and Elastic Beanstalk which leverages EC2, S3, and RDS to enable dynamic webpages</p>
                        </ListGroupItem>
                        <ListGroupItem>
                            <img src="https://cdn.worldvectorlogo.com/logos/react.svg" width="90px" alt="logo" style={{ float: 'left', padding: '10px', paddingRight: '20px' }}/>
                            <a href="https://reactjs.org/"><b>React</b></a>
                            <p>We used the React library to build our user interface and help with pulling data dynamically</p>
                        </ListGroupItem>
                        <ListGroupItem>
                            <img src="https://www.docker.com/sites/default/files/d8/2019-07/Moby-logo.png" width="90px" alt="logo" style={{ float: 'left', padding: '10px', paddingRight: '20px' }}/>
                            <a href="https://www.docker.com/"><b>Docker</b></a>
                            <p>We used Docker to containerize our frontend and backend such that they can be hosted on Elastic Beanstalk</p>
                        </ListGroupItem>
                        <ListGroupItem>
                            <img src="http://stormpath.com/wp-content/uploads/2014/06/flask.jpg" width="90px" alt="logo" style={{ float: 'left', padding: '10px', paddingRight: '20px' }}/>
                            <a href="http://flask.palletsprojects.com/en/1.1.x/"><b>Flask</b></a>
                            <p>We used Flask to help integrate our database</p>
                        </ListGroupItem>
                        <ListGroupItem>
                            <img src="https://miro.medium.com/max/512/1*fVBL9mtLJmHIH6YpU7WvHQ.png" width="90px" alt="logo" style={{ float: 'left', padding: '10px', paddingRight: '20px' }}/>
                            <a href="https://www.getpostman.com/"><b>Postman</b></a>
                            <p>We used Postman to define our website's API and implement calls to fetch data</p>
                        </ListGroupItem>
                    </ListGroup>
                </Container>
                <br></br><br></br>
            </ div>
        );
    }
}
export default About;