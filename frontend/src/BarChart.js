﻿import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import * as d3 from 'd3';


class BarChart extends Component {
    componentDidMount() {
        this.createBarChart();
    }

createBarChart() {
        const data = this.props.data;
        const names = this.props.name;
        const margin = {
            top: 30,
            bottom: 30,
            left: 100,
            right: 50
        }

        let width = 1400;
        let height = 500;
        let x = d3.scaleBand()
            .domain(names)
            .range([margin.left, width - margin.right])
            .padding(0.1);

        let y = d3.scaleLinear()
            .domain([0, d3.max(data)]).nice()
            .range([height - margin.bottom, margin.top]);

        let xAxis = g => g
            .attr("transform", `translate(0,${height - margin.bottom})`)
            .call(d3.axisBottom(x)
                .tickSizeOuter(0));

        let yAxis = g => g
            .attr("transform", `translate(${margin.left},0)`)
            .call(d3.axisLeft(y))
            .call(g => g.select(".domain").remove());


        const svg = d3.select("#body1");

        svg.append("g")
            .style("fill", "#69b3a2")
            .selectAll("rect").data(data).enter().append("rect")
            .attr("x", (d, i) => x(names[i]))
            .attr("y", d => y(d))
            .attr("height", d => y(0) - y(d))
            .attr("width", x.bandwidth());

        svg.append("g")
            .call(xAxis)
            .append('text')
            .attr('x', (this.props.size[0] + 300)/2)
            .attr('y', 45)
            .attr('fill', '#000')
            .style('font-size', '20px')
            .style('text-anchor', 'middle')
            .text('Artist');

        svg.append("g")
            .call(yAxis)
            .append('text')
            .attr('x', 0)
            .attr('y', 0)
            .attr('transform', 'translate(-' + (margin.left - 40) + `, ${this.props.size[1] / 2}) rotate(-90)`)
            .attr('fill', '#000')
            .style('font-size', '20px')
            .style('text-anchor', 'middle')
            .text('Popularity Rankings');
}

    drawChart() {
         
        const data = this.props.data;
        const h = 400; const x = 0;

        const svg = d3.select("#body1").append("svg")
            .attr("width", this.props.width)
            .attr("height", this.props.height);


        svg.selectAll("rect")
            .data(data)
            .enter()
            .append("rect")
            .attr("x", (d, i) => i * 105)
            .attr("y", (d, i) => h - 3 * d)
            .attr("width", 100)
            .attr("height", (d, i) => d * 3)
            .attr("fill", "blue")
        svg.selectAll("text")
            .data(data)
            .enter()
            .append("text")
            .text((d) => d)
            .attr("x", (d, i) => i * 105)
            .attr("y", (d, i) => h - (3 * d) - 5)
    }

    render() {
        return <div align="center">
                <h1>Venue Ratings</h1>
                <svg id='body1'
                    width={1500} height={700}>
                </svg>
            </div>
    }
}

export default BarChart;