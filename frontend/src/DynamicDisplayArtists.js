import React from 'react'
import "react-table/react-table.css"
import ReactTable from "react-table"
import matchSorter from "match-sorter"
import Highlighter from 'react-highlight-words'
import bg from './components/seigaiha.png'

const domain = "http://flask-env.iy8jxgvnfb.us-west-2.elasticbeanstalk.com/api/artists"

export default class DisplayArtist extends React.Component {

	constructor(props) {
		super(props)
		let match = this.props.match
		this.state = {
			data: null, loading: true, albumMin: 0, albumMax: 1000,
			popMin: 0, popMax: 1000, songMin: 0, songMax: 1000, nameKey : "",
			columns: [
				{
					Header: "Artist Name",
					accessor: "name",
					Cell: (rows) => <a href={"/artists/" + rows.value}>{
						<Highlighter
							searchWords={[this.state.nameKey]}
							textToHighlight={rows.value}
						/>}</a>,
					Filter: (filter, onChange) =>
							<input type="text"
							onChange={(event) => this.setState({
								nameKey : event.target.value
								})}
							size="20"
							/>,
					minWidth: 150
				
				},
				{
					Header: "Image",
					accessor: "image",
					Cell: (rows) => {
						if (!rows.value) { return "N/A" }
						else {
							return <img src={rows.value} width="200px" alt="logo" style={{ float: 'left', padding: '10px', paddingRight: '60px' }} />
						}
					},
					width: 160,
					filterable: false
				},
				{
					Header: "Popularity (out of 100)",
					accessor: "popularity",
					minWidth: 200,
					Filter: (filter, onChange) =>
						<div>
							<input type="number"
								onChange={(event) => this.setState({
									popMin: event.target.value
								})}
								placeholder={0}
								size="5"/>
							to
							<input type="number"
								onChange={(event) => this.setState({
									popMax: event.target.value
								})}
								placeholder={1000}
								size="5"/>
						</div>,
					sortMethod: (lhs, rhs) => {
						return lhs > rhs ? 1 : -1
					},
				},
				{
					Header: "# of Top Tracks",
					accessor: "top_songs",
					Cell: (rows) => { return rows.value.length },
					Filter: (filter, onChange) =>
						<div>
							<input type="number"
								onChange={(event) => this.setState({
									songMin: event.target.value
								})}
								placeholder={0} />
							to
							<input type="number"
								onChange={(event) => this.setState({
									songMax: event.target.value
								})}
								placeholder={1000} />
						</div>,
					sortMethod: (lhs, rhs) => {
						return lhs.length > rhs.length ? 1 : -1
					},	
					minWidth: 200
				},
				{
					Header: "# of Albums",
					accessor: "albums",
					minWidth: 200,
					Cell: rows => rows.value.length,
					Filter: (filter, onChange) =>
						<div>
							<input type="number"
								onChange={(event) => this.setState({
									albumMin: event.target.value
								})}
								placeholder={0} />
							to
							<input type="number"
								onChange={(event) => this.setState({
									albumMax: event.target.value
								})}
								placeholder={1000} />
						</div>,
					sortMethod: (lhs, rhs) => {
						return lhs.length > rhs.length ? 1 : -1
					},
				},
				{
					Header: "Genre",
					accessor: "genre",
					Cell: (rows) => {
						return rows.value[0]
					},
					// Change this to filter from drop down
					filterMethod: (filter, row) => {
						if (filter.value === "all")
							return true;
						var i = 0
						var ft = filter.value
						if (filter.value.includes("1")) {
							ft = filter.value.substring(1,
								filter.value.length)
							i++
						}
						else if (!row[filter.id].length
							|| (i == 1 && row[filter.id].length < 2))
							return false
						if (filter.value === "other")
							return !row[filter.id][i].includes("rock")
								&& !row[filter.id][i].includes("pop")
								&& !row[filter.id][i].includes("metal")
								&& !row[filter.id][i].includes("country")
								&& !row[filter.id][i].includes("rap")
						return row[filter.id][i] && row[filter.id][i].includes(ft)
					},
					Filter: ({ filter, onChange }) =>
						<select
							onChange={event => onChange(event.target.value)}
							style={{ width: "100%" }}
							value={filter ? filter.value : "all"}
						>
							<option value="all">Show All</option>
							<option value="pop">Pop</option>
							<option value="rock">Rock</option>
							<option value="metal">Metal</option>
							<option value="country">Country</option>
							<option value="rap">Rap</option>
							<option value="other">Other</option>
						</select >
				},
				{
					Header: "Sub Genre",
					accessor: "genre",
					Cell: (rows) => {
						return rows.value[1]
					},
					/*Since the subGenre uses the same accessor as Genre, it uses
					Genre's filterMethod. HOWEVER, the Filter field can be diffrentated*/
					Filter: ({ filter, onChange }) =>
						<select
							onChange={event => onChange(event.target.value)}
							style={{ width: "100%" }}
							value={filter ? filter.value : "all"}
						>
							<option value="all">Show All</option>
							<option value="1pop">Pop</option>
							<option value="1rock">Rock</option>
							<option value="1metal">Metal</option>
							<option value="1country">Country</option>
							<option value="1rap">Rap</option>
							<option value="1other">Other</option>
						</select >
				}
			]
		}
	}

	componentDidMount() {
		fetch(domain)
			.then(response => response.json())
			.then(obj => {
				for (var instance of obj) {
					if (instance.genre) {
						instance.genre =
							instance.genre.replace(/\'/g, "\"") //Replace all instances of ' with " because the former isn't JSON
						instance.genre = JSON.parse(instance.genre)//Make genre an actual array
					}
					if (instance.albums) {
						/*Albums is actually not a list in data, but a really long string
						 thus, we need to make it a list by adding brackets and qoutes
						 we need to get rid of the initial qoutes so as not to mess up the
						 resulting JSON string's format first, however.*/
						instance.albums =
							instance.albums.replace(/\"/g, "")
						instance.albums =
							instance.albums.replace(/,/g,"\",\"")
						instance.albums =
							instance.albums.concat("\"]")
						instance.albums =
							"[\"".concat(instance.albums)
						console.log(instance.albums)
						instance.albums = JSON.parse(instance.albums)
					}
					if (instance.top_songs) {
						/*Do the same for top_songs.*/
						instance.top_songs =
							instance.top_songs.replace(/\"/g, "")
						instance.top_songs =
							instance.top_songs.replace(/,/g, "\",\"")
						instance.top_songs =
							instance.top_songs.concat("\"]")
						instance.top_songs =
							"[\"".concat(instance.top_songs)
						console.log(instance.top_songs)
						instance.albums = JSON.parse(instance.top_songs)
					}
				}
				return obj
			})
            .then(obj => this.setState({ data: obj, loading: false }))
			}

	render() {
				if(this.state.loading) {
					return (
						<div>Loading Artists...</div>)
				}
				else {
					/* Manual filter for the input filters(min and max)
					filters. Also filters out instances with invalid names.*/
					var filtered = this.state.data.filter((instance) => {
						if (!instance || !instance.name || instance.name == ""
							|| !instance.name.toLowerCase()
								.includes(this.state.nameKey.toLowerCase()))
							return false
						if (instance.albums.length > this.state.albumMax
							|| instance.albums.length < this.state.albumMin)
							return false
						if (instance.popularity > this.state.popMax
							|| instance.popularity < this.state.popMin)
							return false
						if (instance.top_songs.length > this.state.songMax
							|| instance.top_songs.length < this.state.songMin)
							return false
						
						return true
					})
			
			return (
				<div>
                    <ReactTable
						data={filtered}
						filterable
						columns={this.state.columns}
						defaultPageSize={10}
						pageSizeOptions={[10, 20]}
						className="-striped"
						style={{ backgroundImage: 'url('+bg+')' }}
					/>
				</div>
			)
	}
	}
}
