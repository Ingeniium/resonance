import React, { Component } from 'react'
import {
	Card, CardImg, CardText, CardBody,
	CardTitle, CardSubtitle, CardLink, Button, Row, Col
} from 'reactstrap';

//require('node-fetch')
export default class DynamicVenue extends React.Component {

	constructor(props) {
		super(props)
	}


	async componentDidMount() {
		var url = this.props.domain + "/events/venue/" + this.props.params
		await this.props.fetchData(url)
	}

	venueLinks(state) {
		let venue = state.venues
		var links = []
		links.concat(null)
		links.concat(null)
		if (venue.website) {
			links[0] = venue.website
			links[1] = "Check the place out!" 
		}
		return links
	}

	artistLinks(state) {
		let artist = state.artists
		var links = []
		links.concat(null)
		links.concat(null)
		if (artist && artist.name) {
			links[0] = this.props.domain2 + "/artists/" + artist.name
			links[1] = "Associated artists include " + artist.name
		}
		return links
	}

	eventLinks(state) {
		let event = state.events
		var links = []
		links.concat(null)
		links.concat(null)
		if (event) {
			links[0] = this.props.domain2 + "/events/" + event.name
			links[1] = "Events happening at this location : " + event.name
		}
		return links
	}

	artistPic(state) {
		if (state.artists) {
			return state.artists.image
		}
	}

	render() {
		let state = this.props.getState();
		if (!state.loaded) {
			return (<div>Loading...</div>)
		}
		let data = state.venues
		let venueLinks = this.venueLinks(state)
		let artistLinks = this.artistLinks(state)
		let eventLinks = this.eventLinks(state)
		return (
			<div>
				<Row>
					<Col sm="6">
						<Card>
							<CardBody>
								<CardImg top width="100%" src={this.artistPic(state)} />
								<CardTitle>{data.name}</CardTitle>
								<CardSubtitle>{data.address}</CardSubtitle>
								<CardText>
									Phone #: {data.number + "\n"} 
									Google Rating: {data.rating + "\n"}
									Place type: {data.type}
									<CardLink href={venueLinks[0]}><div>{venueLinks[1]}</div></CardLink>
									<CardLink href={artistLinks[0]}><div>{artistLinks[1]}</div></CardLink>
									<CardLink href={eventLinks[0]}><div>{eventLinks[1]}</div></CardLink>
								</CardText>
							</CardBody>
						</Card>
					</Col>
				</Row>
			</div>

		)
	}
}