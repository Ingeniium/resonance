

import React, { Component } from 'react'
import DynamicInstance from './DynamicInstance'
import {
	Card, CardImg, CardText, CardBody,
	CardTitle, CardSubtitle, CardLink, Button, Row, Col
} from 'reactstrap';

//require('node-fetch')
//const domain = "http://flask-env.iy8jxgvnfb.us-west-2.elasticbeanstalk.com/api"
const domain2 = ""
export default class DynamicEvent extends React.Component{

	constructor(props) {
		super(props)
	}

	
	async componentDidMount() {
		//this.props.domain + "/events/name/" + this.props.params//this.props.match.params.event
		var url = this.props.domain + "/events/name/" + this.props.params
		console.log(url)
		await this.props.fetchData(url)
		/*await fetch>(url)
			.then(response => response.json())
			.then(obj => this.props.handleChange({ events: obj[0], idevent: obj[0].idevent }) )
			.then(() => this.props.fetchData())
			.then(() => console.log(this.props.getState().idevent))*/

	}

	venueLinks(state) {
		let venue = state.venues
		var links = []
		links.concat(null)
		links.concat(null)
		if (venue) {
			links[0] = this.props.domain2 + "/venues/" + venue.name
			links[1] = "Takes place at " + venue.name
		}
		return links
	}

	artistLinks(state)
	{
		let artist = state.artists
		var links = []
		links.concat(null)
		links.concat(null)
		if (artist) {
			links[0] = this.props.domain2 + "/artists/" + artist.name
			links[1] = "Featuring artists such as " + artist.name
		}
		return links
	}

	eventLinks(state) {
		let event = state.events
		var links = []
		links.concat(null)
		links.concat(null)
		if (event && event.url) {
			links[0] = event.url
			links[1] = "Check it out at Ticketmaster!"
		}
		return links
	}
	render() {
		let state = this.props.getState()
		if (!state.loaded) {
			return (<div>Loading...</div>)
		}
		else {
			let data = state.events
			let venueLinks = this.venueLinks(state)
			let artistLinks = this.artistLinks(state)
			let eventLinks = this.eventLinks(state)
			return (
				<div>
					<Row>
						<Col sm="6">
							<Card>
								<CardImg top width="100%" src={data.images} alt="Card image cap" />
								<CardBody>
									<CardTitle>{data.name}</CardTitle>
									<CardSubtitle>Occurs on {data.date}</CardSubtitle>
									<CardText>
										<p>Time: {data.time}</p>
										<p>Price: {data.price}$</p>
										<p>Genre: {data.genre}</p>
										<div><a href={venueLinks[0]}>{venueLinks[1]}</a></div>
										<div><a href={artistLinks[0]}>{artistLinks[1]}</a></div>
										<div><a href={eventLinks[0]}>{eventLinks[1]}</a></div>
									</CardText>
								</CardBody>
							</Card>
						</Col>
					</Row>
				</div>

			)
		}
	}
}