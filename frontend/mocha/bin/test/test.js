
import'jsdom-global/register'
import React from 'react'
import expect from 'expect'
import { configure, mount, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import DisplayArtist from './DynamicDisplayArtists'
import DisplayVenues from './DynamicDisplayVenues'
import DisplayEvents from './DynamicDisplayEvents'
import DynamicInstance from './DynamicInstance'
import waitUntil from 'async-wait-until'
configure({ adapter: new Adapter() })
var assert = require('assert');
const fetch = require("node-fetch");


describe('DisplayArtists', function () {
	it('Unittest #1', async function () {
		var wrapper = shallow(<DisplayArtist />)
		expect(wrapper.find('div').text()).toEqual("Loading Artists...")
		await wrapper.instance().componentDidMount()
		expect(wrapper.state('data')[0].name).toEqual('Eagles')
		expect(wrapper.state('data')[0].genre[0]).toEqual('album rock')
	})
})

describe('DisplayEvents', function () {
	it('Unittest #2', async function () {
		var wrapper = shallow(<DisplayEvents />)
		expect(wrapper.find('div').text()).toEqual("Loading Events...")
		await wrapper.instance().componentDidMount()
		expect(wrapper.state('data')[0].genre[0]).toEqual('Rock')

	})
})

describe('DisplayVenue', function () {
	it('Unittest #3', async function () {
		var wrapper = shallow(<DisplayVenues />)
		expect(wrapper.find('div').text()).toEqual("Loading Venues...")
		await wrapper.instance().componentDidMount()
		expect(wrapper.state('data')[1].name).toEqual('House of Blues Dallas')
	})
})

describe('Checking artists The Chainsmokers', function () {
	it('Unittest #4', async function () {
		var wrapper = mount(<DynamicInstance match = {{params: { artist: "The Chainsmokers" } }} type="Artist" />)
		waitUntil(() => wrapper.state('artists') != null)
		var data = wrapper.state('artists')
		expect(data.name).toEqual('The Chainsmokers')
		expect(data.genre).toEqual(['edm', 'electropop', 'pop', 'tropical house'])
	})
})


describe('Checking artist Kevin Gates', function () {
	it('Unittest #5', async function () {
		var wrapper = mount(<DynamicInstance match={{ params: { artist: 'Kevin Gates' } }} type="Artist"/>)
		waitUntil(() => wrapper.state('artists') != null)
		var data = wrapper.state('artists')
		expect(data.name).toEqual('Kevin Gates')
		expect(data.popularity).toEqual(82)
	})
})

describe('Checking event MANA: Rayando El Sol Tour 2019', function () {
	it('Unittest #6', async function () {
		var wrapper = mount(<DynamicInstance match={{ params: { event: 'MANA: Rayando El Sol Tour 2019' } }} type="Event"/>)
		waitUntil(() => wrapper.state('events') != null)
		var data = wrapper.state('events')
		expect(data.name).toEqual('MANA: Rayando El Sol Tour 2019')
		expect(data.venue).toEqual("AT&T Center")
	})
})

describe('Checking event Luke Combs: Beer Never Broke My Heart Tour', function () {
	it('Unittest #7', async function () {
		var wrapper = mount(<DynamicInstance match={{params: {name:'Luke Combs: Beer Never Broke My Heart Tour' } }} type="Event"/>)
		waitUntil(() => wrapper.state('events') != null)
		var data = wrapper.state('events')
		expect(data.name).toEqual('Luke Combs: Beer Never Broke My Heart Tour')
		expect(data.date).toEqual("2019-12-07")
	})
})

describe('Checking venue Dickies Arena', function () {
	it('Unittest #8', async function () {
		var wrapper = mount(<DynamicInstance match={{ params: { name: 'Dickies Arena' } }} type="Venue"/>)
		waitUntil(() => wrapper.state('venues') != null)
		var data = wrapper.state('venues')
		expect(data.name).toEqual('Dickies Arena')
		expect(data.address).toEqual("1911 Montgomery St, Fort Worth, TX 76107, USA")
	})
})

describe('Checking venue Globe Life Field', function () {
	it('Unittest #9', async function () {
		var wrapper = mount(<DynamicInstance match={{ params: { name: 'Globe Life Field' } }} type="Venue"/>)
		waitUntil(() => wrapper.state('venues') != null)
		var data = wrapper.state('venues')
		expect(data.name).toEqual('Globe Life Field')
		expect(data.rating).toEqual(4.4)
	})
})

describe('Checking venue Alamodome', function () {
	it('Unittest #10', async function () {
		var wrapper = mount(<DynamicInstance match={{ params: { name: 'Alamodome' } }} type="Venue"/>)
		waitUntil(() => wrapper.state('venues') != null)
		var data = wrapper.state('venues')
		expect(data.number).toEqual('(210)207-3663')
		expect(data.type).toEqual('stadium')
	})
})

const url = "https://localhost:3000"
