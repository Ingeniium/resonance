
import React, { Component } from 'react'
import {
	Card, CardImg, CardText, CardBody,
	CardTitle, CardSubtitle, CardLink, Button, Row, Col
} from 'reactstrap';

//require('node-fetch')
const domain = "http://flask-env.iy8jxgvnfb.us-west-2.elasticbeanstalk.com/api"
const domain2 = ""
export default class DynamicArtist extends React.Component {

	constructor(props) {
		super(props)
	}


	async componentDidMount() {
		var url = this.props.domain + "/events/artist/" + this.props.params
		await this.props.fetchData(url)
	}

	venueLinks(state) {
		let venue = state.venues
		var links = []
		links.concat(null)
		links.concat(null)
		if (venue) {
			links[0] = this.props.domain2 + "/venues/" + venue.name
			links[1] = "Has an event at " + venue.name
		}
		return links
	}

	artistLinks(state) {
		let artist = state.artists
		var links = []
		links.concat(null)
		links.concat(null)
		if (artist.spotifyurl) {
			links[0] = artist.spotifyurl
			links[1] = "Check out their spotify!"
		}
		return links
	}

	eventLinks(state) {
		let event = state.events
		var links = []
		links.concat(null)
		links.concat(null)
		if (event) {
			links[0] = this.props.domain2 + "/events/" + event.name
			links[1] = "Upcoming events for them include " + event.name
		}
		return links
	}

	render() {
		let state = this.props.getState();
		if (!state.loaded) {
			return (<div>Loading...</div>)
		}
		else {
			let data = state.artists
			let venueLinks = this.venueLinks(state)
			let artistLinks = this.artistLinks(state)
			let eventLinks = this.eventLinks(state)
			return (
				<div>
					<Row>
						<Col sm="6">
							<Card>
								<CardImg top width="100%" src={data.image} alt="Card image cap" />
								<CardBody>
									<CardTitle>{data.name}</CardTitle>
									<CardSubtitle>Popularity rating: {data.popularity}</CardSubtitle>
									<CardText>
										<p>Genres include: {data.genre}</p>
										<p>Albums include: {data.albums}</p>
										<p>Their top songs include: {data.top_songs}</p>
										<CardLink href={venueLinks[0]}><div>{venueLinks[1]}</div></CardLink>
										<CardLink href={artistLinks[0]}><div>{artistLinks[1]}</div></CardLink>
										<CardLink href={eventLinks[0]}><div>{eventLinks[1]}</div></CardLink>
									</CardText>
								</CardBody>
							</Card>
						</Col>
					</Row>
				</div>

			)
		}
	}
}