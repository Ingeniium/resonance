import React from 'react'
import ReactTable from 'react-table'
import Highlighter from 'react-highlight-words'
const domain = "http://flask-env.iy8jxgvnfb.us-west-2.elasticbeanstalk.com/api/events"

export default class DisplayEvents extends React.Component {


    constructor(props) {
        super(props)
        let match = this.props.match
		this.state = {
			data: [], loading: true, priceMin : 0, priceMax: 1000,
			priceSorter : 1, nameKey: "",
			columns : [
				{
					Header: "Event Name",
					accessor: "name",
					minWidth: 300,
					Cell: (rows) => <a href={"/events/" + rows.value}>{
						<Highlighter
							searchWords={[this.state.nameKey]}
							textToHighlight={rows.value}
						/>}</a>,

					Filter: (filter, onChange) =>
						<input type="text"
							onChange={(event) => this.setState({
								nameKey: event.target.value
							})}
						/>
				},
				{
					Header: "IMG",
					accessor: "images",
					Cell: (rows) => {
						if (!rows.value) { return "N/A" }
						else {
							return <img src={rows.value} width="200px" alt="logo" style={{ float: 'left', padding: '10px', paddingRight: '60px' }} />
						}
					}
				},
				{
					Header: "Min Price",
					accessor: "price",
					Cell: (rows) => {
						if (!rows.value) { return "N/A" }
						else { return rows.value[0] }
					},
					Filter: (filter, onChange) =>
						<div>
							<input type="number"
								onChange={(event) => this.setState({
									priceMin: event.target.value
								})}
								placeholder={0} />
							
						</div>,
					minWidth: 300,
					/* Since min and maxPrice have the same accessor,
					   they also have to use the same sortMethod. Hence
					   we use a priceSorter field to choose which data
						to sort based on. This field is changed based on the
						header name in react table's onSortedChange.*/
					sortMethod: (lhs, rhs) =>
					{
						var i = this.state.priceSorter
						return lhs[i] < rhs[i] ? 1 : -1
					}
				},
				{
					Header: "Max Price",
					accessor: "price",
					Cell: (rows) => {
						if (!rows.value) { return "N/A" }
						else { return rows.value[1]}
					},
					Filter: (filter, onChange) =>
						<div>
							<input type="number"
								onChange={(event) => this.setState({
									priceMax: event.target.value
								})}
								placeholder={1000} />
							</div>
						,
					minWidth: 300,

				},
				{
					Header: "Start Date",
					accessor: "date"
				},
				{
					Header: "Genre",
					accessor: "genre",
					filterMethod: (filter, row) => {
						if (filter.value === "all")
							return true;
						return row[filter.id] && row[filter.id].includes(filter.value)
					},
					Filter: ({ filter, onChange }) =>
						<select
							onChange={event => onChange(event.target.value)}
							style={{ width: "100%" }}
							value={filter ? filter.value : "all"}
						>
							<option value="all">Show All</option>
							<option value="Pop">Pop</option>
							<option value="Rock">Rock</option>
							<option value="Metal">Metal</option>
							<option value="Country">Country</option>
							<option value="Latin">Latin</option>
							<option value="R&B">R&B</option>
							<option value="Religious">Religious</option>
						</select >
				},
				{
					Header: "Time",
					accessor: "time",
					Cell: (rows) => {
						if (!rows.value) { return "N/A" }
						else { return rows.value }
					}
				}
			]
		}
    }

    componentDidMount() {
        fetch(domain)
			.then(response => response.json())
			.then(obj => {
				for (var instance of obj) {
					if (instance.price) {
						instance.price =
							instance.price.split(" - ")
						instance.price[0] = 
							parseInt(instance.price[0])
						instance.price[1] =
							parseInt(instance.price[1])
					}
				}
				return obj
			})
            .then(obj => this.setState({ data: obj, loading: false }))
    }

	render() {
		if (this.state.loading) {
			return <div>Loading Events...</div>
		}
		var filtered = this.state.data.filter((instance) => {
			if (!instance || !instance.name || instance.name == ""
				|| !instance.name.toLowerCase().
					includes(this.state.nameKey.toLowerCase()))
				return false
			if (!instance.price || instance.price[0] < this.state.priceMin
				|| instance.price[1] > this.state.priceMax)
				return false
			return true
		})
			
		return (
			<div>
				<ReactTable
					data={filtered}
					filterable
					columns={this.state.columns}
					onSortedChange={(newSorted, column, shiftKey) => {
						console.log(column.Header)
						if (column.Header == "Min Price")
							this.setState({ priceSorter: 0 })
						else if (column.Header == "Max Price")
							this.setState({ priceSorter: 1 })
					}}
					defaultPageSize={9}
					pageSizeOptions={[9, 18]}
				/>
			</div>
		)
	}
}