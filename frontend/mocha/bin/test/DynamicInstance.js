import React, { Component } from 'react'
import {
	Card, CardImg, CardText, CardBody,
	CardTitle, CardSubtitle, CardLink, Button, Row, Col
} from 'reactstrap';
import DynamicEvent from  './DynamicEvent'
import DynamicVenue from './DynamicVenue'
import DynamicArtist from './DynamicArtist'
//require('node-fetch')
const domain = "http://flask-env.iy8jxgvnfb.us-west-2.elasticbeanstalk.com/api"
const domain2 = "http://localhost:3000"
export default class DynamicInstance extends React.Component {
	constructor(props) {
		super(props)
		this.state = { loaded: false, idevent: null, events: null, artists: null, venues: null }

		let match = this.props.match
	}

	domain() { return domain }
	domain2() { return domain2 }

	async fetchData(url) {
		console.log("Parent Called")
		console.log(url)
		await fetch(url)
			.then(response => response.json())
			.then(event => this.setState({ events: event[0], artists: event[0].artist, venues: event[0].venue }))
			.catch(console.log("Error getting event data!"))
		console.log(this.state.events)
		console.log(this.state.artists)
		console.log(this.state.events.venue)
		if (this.state.artists) {
			try {
				var url = domain + "/artists/name/" + this.state.events.artist
				console.log("Getting artist data... : " + url)
				await fetch(url)
					.then(response => response.json())
					.then(artist => this.setState({ artists: artist[0] }))
					.catch(console.log("Error getting artist data!"))
			}
			catch (e) { console.error("Error while getting artist data : " + e)}
		}
		if (this.state.venues) {
			try {
				var url = domain + "/venues/name/" + this.state.events.venue
				console.log("Getting venue data... : " + url)
				await fetch(url)
					.then(response => response.json())
					.then(venue => this.setState({ venues: venue[0] }))
					.catch(console.log("Error getting venue data!"))
			}
			catch (e) { console.error("Error getting venue data : " + e) }
		}
		console.log(this.state.events == null || this.state.venues == null || this.state.artists == null)
		this.setState({ loaded: true })
	}

	handleChange(changes) {
		console.log("Parent state changes : " + changes.idevent)
		this.setState(changes)
	}

	getState() {
		return this.state
	}

	render() {
		var param = null
		if (this.props.type == "Event") {
			return (<DynamicEvent params={this.props.match.params.event}
				domain={this.domain()}
				domain2={this.domain2()}
				handleChange={this.handleChange.bind(this)}
				fetchData={this.fetchData.bind(this)}
				getState={this.getState.bind(this)} />)
		}
		else if (this.props.type == "Artist") {
			return (<DynamicArtist params={this.props.match.params.artist}
				domain={this.domain()}
				domain2={this.domain2()}
				handleChange={this.handleChange.bind(this)}
				fetchData={this.fetchData.bind(this)}
				getState={this.getState.bind(this)} />)
		}
		else {
			return (<DynamicVenue params={this.props.match.params.venue}
				domain={this.domain()}
				domain2={this.domain2()}
				handleChange={this.handleChange.bind(this)}
				fetchData={this.fetchData.bind(this)}
				getState={this.getState.bind(this)} />)
		}

	}
	
	
}